package com.satish.fieldvalidator.datajpaadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataJpaAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataJpaAdapterApplication.class, args);
	}

}
