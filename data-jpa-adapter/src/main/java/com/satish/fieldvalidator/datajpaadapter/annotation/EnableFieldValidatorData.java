package com.satish.fieldvalidator.datajpaadapter.annotation;

import com.satish.fieldvalidator.datajpaadapter.DataJpaAdapterApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Import(DataJpaAdapterApplication.class)
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableFieldValidatorData {
}
