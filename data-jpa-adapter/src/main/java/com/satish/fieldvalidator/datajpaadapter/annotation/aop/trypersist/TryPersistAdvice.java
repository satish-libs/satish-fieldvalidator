package com.satish.fieldvalidator.datajpaadapter.annotation.aop.trypersist;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.domain.exceptions.MiscException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TryPersistAdvice {

    @Around("@annotation(com.satish.fieldvalidator.datajpaadapter.annotation.aop.trypersist.TryPersist) && execution(* *(..))")
    public Object tryCatchAround(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            return joinPoint.proceed();
        } catch (DataIntegrityViolationException dataIntVioExc) {
            String message = dataIntVioExc.getRootCause() != null ?
                    dataIntVioExc.getRootCause().getMessage() :
                    "Data integrity violation exception";
            throw new DataConflictException(message);
        } catch (Exception exc) {
            throw new MiscException(exc.getMessage());
        }
    }
}
