package com.satish.fieldvalidator.datajpaadapter.configuration;

import com.satish.common.crudop.repositorylayer.baserepository.impl.BaseRepositoryImpl;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages ="com.satish.fieldvalidator.datajpaadapter")
@EntityScan(basePackages = "com.satish.fieldvalidator.datajpaadapter.entity")
@EnableJpaRepositories(basePackages = "com.satish.fieldvalidator.datajpaadapter.repository", repositoryBaseClass = BaseRepositoryImpl.class)
public class FieldValidatorDrivenConfiguration {

}
