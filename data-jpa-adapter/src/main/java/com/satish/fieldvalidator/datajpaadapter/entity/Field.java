package com.satish.fieldvalidator.datajpaadapter.entity;

import com.satish.fieldvalidator.datajpaadapter.entity.base.Identity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tbl_field")
@Getter
@Setter
@ToString(exclude = "fieldValidators")
@NoArgsConstructor
public final class Field extends Identity {

    @Column(unique = true, nullable = false)
    private String fieldKey;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String label;

    @Column
    private String placeholder;

    @Column(nullable = false, columnDefinition = " bit default 1")
    private boolean status;

    @OneToMany(mappedBy = "field", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<FieldValidator> fieldValidators;

    public Field(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return Objects.equals(getId(), field.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
