package com.satish.fieldvalidator.datajpaadapter.entity;

import com.satish.fieldvalidator.datajpaadapter.entity.base.Identity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;


@SqlResultSetMapping(
        name = "messageResponseWithValue",
        classes = @ConstructorResult(
                targetClass = MessageValidatorValue.class,
                columns = {
                        @ColumnResult(name = "code"),
                        @ColumnResult(name = "message"),
                        @ColumnResult(name = "value"),
                        @ColumnResult(name = "type")

                }
        )
)

@NamedNativeQuery(name = "getAllMessageResponseWithValue",
        resultClass = MessageValidatorValue.class,
        resultSetMapping = "messageResponseWithValue",
        query = "select vv.value, mr.code, mr.message, tv.type from tbl_field f " +
                " join tbl_field_validator fv " +
                " on f.id = fv.field_id " +
                " join tbl_validator tv " +
                " on tv.id = fv.validator_id " +
                " join tbl_validator_value vv " +
                " on vv.id = fv.validator_value_id " +
                " join tbl_message_response mr " +
                " on mr.id = fv.message_response_id " +
                " where f.field_key = :fkey" +
                " and tv.type  in :types")
@Entity
@Table(name = "tbl_field_validator")
@Getter
@Setter
@ToString(exclude = {"field", "messageResponse", "validator", "validatorValue"})
public final class FieldValidator extends Identity {

    @ManyToOne
    @JoinColumn(name = "field_id")
    private Field field;

    @ManyToOne
    @JoinColumn(name = "message_response_id")
    private MessageResponse messageResponse;

    @ManyToOne
    @JoinColumn(name  = "validator_id")
    private Validator validator;

    @ManyToOne
    @JoinColumn(name  = "validator_value_id")
    private ValidatorValue validatorValue;

    @Column(nullable = false)
    private boolean status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FieldValidator that = (FieldValidator) o;
        return  Objects.equals(field, that.field) &&
                Objects.equals(messageResponse, that.messageResponse) &&
                Objects.equals(validator, that.validator) &&
                Objects.equals(validatorValue, that.validatorValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, messageResponse, validator, validatorValue, status);
    }
}
