package com.satish.fieldvalidator.datajpaadapter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class FieldValidatorID implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int id;

    private int fieldId;

    private int messageResponseId;

    private int validatorId;

    private int validatorValueId;
}
