package com.satish.fieldvalidator.datajpaadapter.entity;

import com.satish.fieldvalidator.datajpaadapter.entity.base.Identity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tbl_message_response")
@Getter
@Setter
@ToString(exclude = "fieldValidators")
public final class MessageResponse extends Identity {

    @Column(unique = true, nullable = false)
    private String message;

    @Column(unique = true, nullable = false, updatable = false)
    private int code;

    @OneToMany(mappedBy = "messageResponse", fetch = FetchType.LAZY)
    private Set<FieldValidator> fieldValidators;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageResponse messageResponse = (MessageResponse) o;
        return Objects.equals(getId(), messageResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
