package com.satish.fieldvalidator.datajpaadapter.entity;

import java.util.Objects;


public class MessageValidatorValue{
    private Long id;
    private int code;
    private String message;
    private String value;
    private String type;

    public MessageValidatorValue() {
    }
    public MessageValidatorValue(int code, String message, String value, String type) {
        this.code = code;
        this.message = message;
        this.value = value;
        this.type = type;
    }
    public MessageValidatorValue(Long id, int code, String message, String value, String type) {
        this.id = id;
        this.code = code;
        this.message = message;
        this.value = value;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageValidatorValue that = (MessageValidatorValue) o;
        return Objects.equals(code, that.code);

    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String toString() {
        return "MessageValidatorValue{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
