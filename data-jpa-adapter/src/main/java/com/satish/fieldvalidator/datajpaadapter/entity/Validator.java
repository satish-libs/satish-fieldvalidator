package com.satish.fieldvalidator.datajpaadapter.entity;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.datajpaadapter.entity.base.Identity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tbl_validator")
@Getter
@Setter
@ToString(exclude = "fieldValidators")
@Check(constraints = "type IN ('MAX_LENGTH', 'MIN_LENGTH', 'MAX_VALUE', 'MIN_VALUE', 'REQUIRED', 'PATTERN')")
public final class Validator extends Identity {

    @Column(unique = true, nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ValidatorType type;

    @Column(nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ValidatorDataType dataType;

    @OneToMany(mappedBy = "validator", fetch = FetchType.LAZY)
    private Set<FieldValidator> fieldValidators;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Validator validator = (Validator) o;
        return Objects.equals(getId(), validator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
