package com.satish.fieldvalidator.datajpaadapter.entity;

import com.satish.fieldvalidator.datajpaadapter.entity.base.Identity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tbl_validator_value")
@Getter
@Setter
@ToString(exclude = "fieldValidators")
public final class ValidatorValue extends Identity {

    @Column(unique = true)
    private String value;

    @OneToMany(mappedBy = "validatorValue", fetch = FetchType.LAZY)
    private Set<FieldValidator> fieldValidators;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorValue validatorValue = (ValidatorValue) o;
        return Objects.equals(getId(), validatorValue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
