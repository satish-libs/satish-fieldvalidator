package com.satish.fieldvalidator.datajpaadapter.exceptions;

public class ErrorCodeConstants {
    private ErrorCodeConstants() {}

    public static final String USER_NOT_FOUND = "Incorrect user name or password";
    public static final String MESSAGE_RESPONSE_NOT_FOUND = "Message response not found";
    public static final String VALIDATOR_NOT_FOUND = "Validator not found";
    public static final String VALIDATOR_VALUE_NOT_FOUND = "Validator value not found";
}
