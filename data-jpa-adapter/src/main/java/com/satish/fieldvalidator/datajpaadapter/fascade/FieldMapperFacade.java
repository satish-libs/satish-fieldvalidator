package com.satish.fieldvalidator.datajpaadapter.fascade;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.*;
import com.satish.fieldvalidator.datajpaadapter.mapper.*;
import com.satish.fieldvalidator.domain.domain.*;

public interface FieldMapperFacade {
    MessageResponseMapper getMessageResponseMapper();

    ValidatorValueMapper getValidatorValueMapper();

    FieldValidatorMapper getFieldValidatorMapper();

    ValidatorMapper getValidatorMapper();

    FieldMapper getFieldMapper();

    CollectionMapper<MessageResponseDomain, MessageResponse> mapMessageResponses();

    CollectionMapper<ValidatorValueDomain, ValidatorValue> mapValidatorValues();

    CollectionMapper<ValidatorDomain, Validator> mapValidators();

    CollectionMapper<FieldDomain, Field> mapFields();

    CollectionMapper<FieldValidatorDomain, FieldValidator> mapFieldValidators();
}
