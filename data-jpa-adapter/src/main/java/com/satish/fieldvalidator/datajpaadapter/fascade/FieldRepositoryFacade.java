package com.satish.fieldvalidator.datajpaadapter.fascade;

import com.satish.fieldvalidator.datajpaadapter.repository.*;

public interface FieldRepositoryFacade {
    FieldRepository getFieldRepository();
    FieldValidatorRepository getFieldValidatorRepository();
    MessageResponseRepository getMessageResponseRepository();
    ValidatorValueRepository getValidatorValueRepository();
    ValidatorRepository getValidatorRepository();
}
