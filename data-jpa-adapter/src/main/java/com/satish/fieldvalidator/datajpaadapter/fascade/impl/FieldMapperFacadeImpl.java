package com.satish.fieldvalidator.datajpaadapter.fascade.impl;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.concrete.CollectionMapperFactory;
import com.satish.common.util.CollectionKey;
import com.satish.fieldvalidator.datajpaadapter.entity.*;
import com.satish.fieldvalidator.datajpaadapter.fascade.FieldMapperFacade;
import com.satish.fieldvalidator.datajpaadapter.mapper.*;
import com.satish.fieldvalidator.domain.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FieldMapperFacadeImpl implements FieldMapperFacade {
    private final FieldMapper fieldMapper;
    private final MessageResponseMapper messageResponseMapper;
    private final ValidatorValueMapper validatorValueMapper;
    private final ValidatorMapper validatorMapper;
    private final FieldValidatorMapper fieldValidatorMapper;
    @Override
    public MessageResponseMapper getMessageResponseMapper() {
        return messageResponseMapper;
    }

    @Override
    public ValidatorValueMapper getValidatorValueMapper() {
        return validatorValueMapper;
    }

    @Override
    public FieldValidatorMapper getFieldValidatorMapper() {
        return fieldValidatorMapper;
    }

    @Override
    public ValidatorMapper getValidatorMapper() {
        return validatorMapper;
    }

    @Override
    public FieldMapper getFieldMapper() {
        return fieldMapper;
    }

    @Override
    public CollectionMapper<MessageResponseDomain, MessageResponse> mapMessageResponses() {
        return CollectionMapperFactory.getMapper(CollectionKey.LIST, messageResponseMapper);
    }

    @Override
    public CollectionMapper<ValidatorValueDomain, ValidatorValue> mapValidatorValues() {
        return CollectionMapperFactory.getMapper(CollectionKey.LIST, validatorValueMapper);
    }

    @Override
    public CollectionMapper<ValidatorDomain, Validator> mapValidators() {
        return CollectionMapperFactory.getMapper(CollectionKey.LIST, validatorMapper);
    }

    @Override
    public CollectionMapper<FieldDomain, Field> mapFields() {
        return CollectionMapperFactory.getMapper(CollectionKey.LIST, fieldMapper);
    }

    @Override
    public CollectionMapper<FieldValidatorDomain, FieldValidator> mapFieldValidators() {
        return CollectionMapperFactory.getMapper(CollectionKey.LIST, fieldValidatorMapper);
    }
}
