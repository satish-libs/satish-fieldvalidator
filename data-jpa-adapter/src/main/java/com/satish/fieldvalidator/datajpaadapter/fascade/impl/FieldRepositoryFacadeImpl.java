package com.satish.fieldvalidator.datajpaadapter.fascade.impl;

import com.satish.fieldvalidator.datajpaadapter.fascade.FieldRepositoryFacade;
import com.satish.fieldvalidator.datajpaadapter.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FieldRepositoryFacadeImpl implements FieldRepositoryFacade {
    private final FieldRepository fieldRepository;
    private final MessageResponseRepository messageResponseRepository;
    private final ValidatorRepository validatorRepository;
    private final ValidatorValueRepository validatorValueRepository;
    private final FieldValidatorRepository fieldValidatorRepository;
    @Override
    public FieldRepository getFieldRepository() {
        return fieldRepository;
    }

    @Override
    public FieldValidatorRepository getFieldValidatorRepository() {
        return fieldValidatorRepository;
    }

    @Override
    public MessageResponseRepository getMessageResponseRepository() {
        return messageResponseRepository;
    }

    @Override
    public ValidatorValueRepository getValidatorValueRepository() {
        return validatorValueRepository;
    }

    @Override
    public ValidatorRepository getValidatorRepository() {
        return validatorRepository;
    }
}
