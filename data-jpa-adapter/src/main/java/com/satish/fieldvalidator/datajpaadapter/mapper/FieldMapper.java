package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.Field;
import com.satish.fieldvalidator.domain.domain.FieldDomain;

public interface FieldMapper extends AbstractMapper<FieldDomain, Field> {
}
