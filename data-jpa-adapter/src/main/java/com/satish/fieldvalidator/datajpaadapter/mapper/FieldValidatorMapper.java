package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.Field;
import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;

public interface FieldValidatorMapper extends AbstractMapper<FieldValidatorDomain, FieldValidator> {
    Field toField(FieldDomain fieldDomain);
}
