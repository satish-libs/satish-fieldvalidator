package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageResponse;
import com.satish.fieldvalidator.domain.domain.MessageResponseDomain;

public interface MessageResponseMapper extends AbstractMapper<MessageResponseDomain, MessageResponse> {
}
