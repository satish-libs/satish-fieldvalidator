package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.fieldvalidator.datajpaadapter.entity.MessageValidatorValue;
import com.satish.fieldvalidator.domain.domain.MessageValidatorValueDomain;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface MessageValidatorMapper {
    Set<MessageValidatorValue> toEntities(Set<MessageValidatorValueDomain> messageValidatorValueDomain);

    Set<MessageValidatorValueDomain> toDomains(Set<MessageValidatorValue> messageValidatorValue);
}
