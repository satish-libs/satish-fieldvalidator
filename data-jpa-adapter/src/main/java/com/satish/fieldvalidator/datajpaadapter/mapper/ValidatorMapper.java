package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.Validator;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;

public interface ValidatorMapper extends AbstractMapper<ValidatorDomain, Validator> {
}
