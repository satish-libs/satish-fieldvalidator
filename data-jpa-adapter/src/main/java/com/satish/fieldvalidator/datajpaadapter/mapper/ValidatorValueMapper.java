package com.satish.fieldvalidator.datajpaadapter.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.datajpaadapter.entity.ValidatorValue;
import com.satish.fieldvalidator.domain.domain.ValidatorValueDomain;

public interface ValidatorValueMapper extends AbstractMapper<ValidatorValueDomain, ValidatorValue> {
}
