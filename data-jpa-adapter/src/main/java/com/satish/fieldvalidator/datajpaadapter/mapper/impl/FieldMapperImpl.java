package com.satish.fieldvalidator.datajpaadapter.mapper.impl;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.concrete.CollectionMapperFactory;
import com.satish.common.util.CollectionKey;
import com.satish.fieldvalidator.datajpaadapter.entity.Field;
import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import com.satish.fieldvalidator.datajpaadapter.mapper.FieldMapper;
import com.satish.fieldvalidator.datajpaadapter.mapper.FieldValidatorMapper;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class FieldMapperImpl implements FieldMapper {
    private CollectionMapper<FieldValidatorDomain, FieldValidator> fieldValidatorCollection;

    public FieldMapperImpl(final FieldValidatorMapper fieldValidatorMapper) {
        this.fieldValidatorCollection = CollectionMapperFactory.getMapper(CollectionKey.SET, fieldValidatorMapper);
    }

    @Override
    public FieldDomain toTarget(Field source) {
        if (source == null) {
            return null;
        }
        FieldDomain fieldDomain = new FieldDomain();
        fieldDomain.setId(source.getId());
        fieldDomain.setStatus(source.isStatus());
        fieldDomain.setPlaceholder(source.getPlaceholder());
        fieldDomain.setName(source.getName());
        fieldDomain.setLabel(source.getLabel());
        fieldDomain.setFieldKey(source.getFieldKey());
        fieldDomain.setFieldValidators((Set<FieldValidatorDomain>) fieldValidatorCollection
                .toTargets(source.getFieldValidators()));
        return fieldDomain;
    }

    @Override
    public Field toSource(FieldDomain target) {
        if (target == null) {
            return null;
        }
        Field field = new Field();
        field.setId(target.getId());
        field.setStatus(target.getId() <= 0 || target.isStatus());
        field.setPlaceholder(target.getPlaceholder());
        field.setName(target.getName());
        field.setLabel(target.getLabel());
        field.setFieldKey(target.getFieldKey());
        Set<FieldValidator> fieldValidators = (Set<FieldValidator>)fieldValidatorCollection
                .toSources(target.getFieldValidators());
        fieldValidators.forEach(fieldValidator -> fieldValidator.setField(field));
        field.setFieldValidators(fieldValidators);
        return field;
    }
}
