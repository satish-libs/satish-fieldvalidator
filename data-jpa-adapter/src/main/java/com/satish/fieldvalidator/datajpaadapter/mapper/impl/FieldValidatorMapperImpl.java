package com.satish.fieldvalidator.datajpaadapter.mapper.impl;

import com.satish.fieldvalidator.datajpaadapter.entity.Field;
import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import com.satish.fieldvalidator.datajpaadapter.mapper.FieldValidatorMapper;
import com.satish.fieldvalidator.datajpaadapter.mapper.MessageResponseMapper;
import com.satish.fieldvalidator.datajpaadapter.mapper.ValidatorMapper;
import com.satish.fieldvalidator.datajpaadapter.mapper.ValidatorValueMapper;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FieldValidatorMapperImpl implements FieldValidatorMapper {
    private final MessageResponseMapper messageResponseMapper;
    private final ValidatorValueMapper validatorValueMapper;
    private final ValidatorMapper validatorMapper;
    @Override
    public FieldValidatorDomain toTarget(FieldValidator source) {
        if(source == null) {
            return null;
        }
        FieldValidatorDomain fieldValidatorDomain = new FieldValidatorDomain();
        fieldValidatorDomain.setId(source.getId());
        fieldValidatorDomain.setMessageResponse(messageResponseMapper.toTarget(source.getMessageResponse()));
        fieldValidatorDomain.setValidatorValue(validatorValueMapper.toTarget(source.getValidatorValue()));
        fieldValidatorDomain.setValidator(validatorMapper.toTarget(source.getValidator()));
        fieldValidatorDomain.setStatus(source.isStatus());
        return fieldValidatorDomain;
    }

    @Override
    public FieldValidator toSource(FieldValidatorDomain target) {
        if(target == null) {
            return null;
        }
        FieldValidator fieldValidator = new FieldValidator();
        fieldValidator.setMessageResponse(messageResponseMapper.toSource(target.getMessageResponse()));
        fieldValidator.setValidatorValue(validatorValueMapper.toSource(target.getValidatorValue()));
        fieldValidator.setValidator(validatorMapper.toSource(target.getValidator()));
        fieldValidator.setField(toField(target.getField()));
        fieldValidator.setId(target.getId());
        fieldValidator.setStatus(target.getId() <= 0 || target.isStatus());
        return fieldValidator;
    }

    @Override
    public Field toField(FieldDomain fieldDomain) {
        if(fieldDomain == null) {
            return null;
        }
        Field field = new Field();
        field.setId(fieldDomain.getId());
        field.setStatus(fieldDomain.isStatus());
        field.setPlaceholder(fieldDomain.getPlaceholder());
        field.setName(fieldDomain.getName());
        field.setLabel(fieldDomain.getLabel());
        field.setFieldKey(fieldDomain.getFieldKey());
        return field;
    }
}
