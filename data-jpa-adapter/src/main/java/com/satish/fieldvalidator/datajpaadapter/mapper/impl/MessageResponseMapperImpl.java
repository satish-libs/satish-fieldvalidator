package com.satish.fieldvalidator.datajpaadapter.mapper.impl;

import com.satish.fieldvalidator.datajpaadapter.entity.MessageResponse;
import com.satish.fieldvalidator.datajpaadapter.mapper.MessageResponseMapper;
import com.satish.fieldvalidator.domain.domain.MessageResponseDomain;
import org.springframework.stereotype.Service;

@Service
public class MessageResponseMapperImpl implements MessageResponseMapper {
    @Override
    public MessageResponseDomain toTarget(MessageResponse source) {
        if(source == null) {
            return null;
        }
        MessageResponseDomain messageResponseDomain = new MessageResponseDomain();
        messageResponseDomain.setCode(source.getCode());
        messageResponseDomain.setMessage(source.getMessage());
        messageResponseDomain.setId(source.getId());
        return messageResponseDomain;
    }

    @Override
    public MessageResponse toSource(MessageResponseDomain target) {
        if(target == null) {
            return null;
        }
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode(target.getCode());
        messageResponse.setMessage(target.getMessage());
        messageResponse.setId(target.getId());
        return messageResponse;
    }
}
