package com.satish.fieldvalidator.datajpaadapter.mapper.impl;

import com.satish.fieldvalidator.datajpaadapter.entity.Validator;
import com.satish.fieldvalidator.datajpaadapter.mapper.ValidatorMapper;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import org.springframework.stereotype.Service;

@Service
public class ValidatorMapperImpl implements ValidatorMapper {
    @Override
    public ValidatorDomain toTarget(Validator source) {
        if(source == null) {
            return null;
        }
        ValidatorDomain validatorDomain = new ValidatorDomain();
        validatorDomain.setType(source.getType());
        validatorDomain.setDataType(source.getDataType());
        validatorDomain.setId(source.getId());
        return validatorDomain;
    }

    @Override
    public Validator toSource(ValidatorDomain target) {
        if(target == null) {
            return null;
        }
        Validator validator = new Validator();
        validator.setType(target.getType());
        validator.setDataType(target.getDataType());
        validator.setId(target.getId());
        return validator;
    }
}
