package com.satish.fieldvalidator.datajpaadapter.mapper.impl;

import com.satish.fieldvalidator.datajpaadapter.entity.ValidatorValue;
import com.satish.fieldvalidator.datajpaadapter.mapper.ValidatorValueMapper;
import com.satish.fieldvalidator.domain.domain.ValidatorValueDomain;
import org.springframework.stereotype.Service;

@Service

public class ValidatorValueMapperImpl implements ValidatorValueMapper {
    @Override
    public ValidatorValueDomain toTarget(ValidatorValue source) {
        if(source == null) {
            return null;
        }
        ValidatorValueDomain validatorValueDomain = new ValidatorValueDomain();
        validatorValueDomain.setValue(source.getValue());
        validatorValueDomain.setId(source.getId());
        return validatorValueDomain;
    }

    @Override
    public ValidatorValue toSource(ValidatorValueDomain target) {
        if(target == null) {
            return null;
        }
        ValidatorValue validatorValue = new ValidatorValue();
        validatorValue.setValue(target.getValue());
        validatorValue.setId(target.getId());
        return validatorValue;
    }
}
