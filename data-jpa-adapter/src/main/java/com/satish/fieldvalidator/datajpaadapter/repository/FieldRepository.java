package com.satish.fieldvalidator.datajpaadapter.repository;

import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.fieldvalidator.datajpaadapter.entity.Field;

import java.util.List;
import java.util.Optional;

public interface FieldRepository extends DrivenAdapterRepository<Field, Integer> {
    Optional<List<Field>> findByStatusTrue();
}
