package com.satish.fieldvalidator.datajpaadapter.repository;

import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import org.springframework.data.jpa.repository.query.Procedure;

public interface FieldValidatorRepository extends DrivenAdapterRepository<FieldValidator, Integer> {
    @Procedure(procedureName = "PROC_UPDATE_MESSAGE_RESPONSE_OF_FIELD")
    void updateMessageResponseOfField(int fieldValidatorId, String newMessage);

    @Procedure(procedureName = "PROC_UPDATE_VALIDATOR_VALUE_OF_FIELD")
    void updateValidatorValueOfField(int fieldValidatorId, String newValue);
}
