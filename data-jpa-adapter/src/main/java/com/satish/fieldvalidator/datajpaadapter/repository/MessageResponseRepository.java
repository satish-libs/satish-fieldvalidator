package com.satish.fieldvalidator.datajpaadapter.repository;

import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageResponse;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface MessageResponseRepository extends DrivenAdapterRepository<MessageResponse, Integer> {
    @Query("Select MAX(m.code) from MessageResponse m where m.code >= 5000")
    Optional<Integer> getMaxCode();
    Optional<MessageResponse> findByIdOrMessage(int id, String message);
}
