package com.satish.fieldvalidator.datajpaadapter.repository;

import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageValidatorValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface MessageValidatorValueRepository extends JpaRepository<FieldValidator, Long> {
    @Query(name = "getAllMessageResponseWithValue", nativeQuery = true)
    Set<MessageValidatorValue> getMessageResponseByFieldNameAndValidators(@Param("fkey") String fieldKey, @Param("types") List<String> validators);
}
