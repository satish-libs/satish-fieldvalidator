package com.satish.fieldvalidator.datajpaadapter.repository;


import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.fieldvalidator.datajpaadapter.entity.Validator;

public interface ValidatorRepository extends DrivenAdapterRepository<Validator, Integer> {
}
