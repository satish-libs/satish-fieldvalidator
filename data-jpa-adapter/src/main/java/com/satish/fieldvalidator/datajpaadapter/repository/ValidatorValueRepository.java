package com.satish.fieldvalidator.datajpaadapter.repository;

import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.fieldvalidator.datajpaadapter.entity.ValidatorValue;

import java.util.Optional;

public interface ValidatorValueRepository extends DrivenAdapterRepository<ValidatorValue, Integer> {
    Optional<ValidatorValue> findByIdOrValue(int id, String value);
}
