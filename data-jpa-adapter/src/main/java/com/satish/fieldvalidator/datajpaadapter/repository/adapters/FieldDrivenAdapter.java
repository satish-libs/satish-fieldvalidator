package com.satish.fieldvalidator.datajpaadapter.repository.adapters;

import com.satish.common.crudop.domainlayer.CRUDAbstract;
import com.satish.common.crudop.repositorylayer.impl.DrivenAdapterRepositoryImpl;
import com.satish.fieldvalidator.datajpaadapter.annotation.aop.trypersist.TryPersist;
import com.satish.fieldvalidator.datajpaadapter.entity.Field;
import com.satish.fieldvalidator.datajpaadapter.entity.FieldValidator;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageResponse;
import com.satish.fieldvalidator.datajpaadapter.entity.ValidatorValue;
import com.satish.fieldvalidator.datajpaadapter.fascade.FieldMapperFacade;
import com.satish.fieldvalidator.datajpaadapter.fascade.FieldRepositoryFacade;
import com.satish.fieldvalidator.datajpaadapter.mapper.FieldMapper;
import com.satish.fieldvalidator.datajpaadapter.repository.FieldRepository;
import com.satish.fieldvalidator.datajpaadapter.repository.MessageResponseRepository;
import com.satish.fieldvalidator.datajpaadapter.repository.ValidatorValueRepository;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.spi.FieldDrivenPort;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional
@Repository
@Primary
public class FieldDrivenAdapter extends CRUDAbstract<FieldDomain, Integer> implements FieldDrivenPort {

    private FieldRepository fieldRepository;
    private MessageResponseRepository messageResponseRepository;
    private ValidatorValueRepository validatorValueRepository;
    private FieldMapper fieldMapper;

    public FieldDrivenAdapter(final FieldRepositoryFacade fieldRepositoryFacade,
                              final FieldMapperFacade fieldMapperFacade
    ) {
        super(new DrivenAdapterRepositoryImpl<>(fieldRepositoryFacade.getFieldRepository(), fieldMapperFacade.getFieldMapper()));
        this.fieldRepository = fieldRepositoryFacade.getFieldRepository();
        this.messageResponseRepository = fieldRepositoryFacade.getMessageResponseRepository();
        this.validatorValueRepository = fieldRepositoryFacade.getValidatorValueRepository();
        this.fieldMapper = fieldMapperFacade.getFieldMapper();
    }

    @TryPersist
    @Override
    public FieldDomain updateColumnBy(Integer id, String columnName, String value) {
        return fieldMapper.toTarget(fieldRepository.updateColumnBy(id, columnName, value));
    }

    @TryPersist
    @Override
    public FieldDomain save(FieldDomain fieldDomain) {
        Field field = fieldMapper.toSource(fieldDomain);
        if (!field.getFieldValidators().isEmpty()) {
            field.getFieldValidators().forEach(this::setUpdatedFieldValidator);
        }
        return fieldMapper.toTarget(fieldRepository.saveAndFlush(field));
    }


    @Override
    public List<FieldDomain> getActiveFields() {
        List<Field> fields = fieldRepository.findByStatusTrue().orElse(Collections.emptyList());
        fields.forEach(field -> field.setFieldValidators(filterFieldValidatorBy(field.getFieldValidators())));
        return fieldMapper
                .toTargetList(fields);
    }

    private Set<FieldValidator> filterFieldValidatorBy(Set<FieldValidator> fieldValidators) {
        return fieldValidators.stream()
                .filter(FieldValidator::isStatus)
                .collect(Collectors.toSet());
    }

    private void setUpdatedFieldValidator(FieldValidator oldFieldValidator) {
        oldFieldValidator.setMessageResponse(messageResponseRepository.save(getMessageResponse(oldFieldValidator)));
        oldFieldValidator.setValidatorValue(validatorValueRepository.save(getValidatorValue(oldFieldValidator)));
    }

    private MessageResponse getMessageResponse(FieldValidator fieldValidator) {
        MessageResponse newMessageResponse = fieldValidator.getMessageResponse();
        Optional<MessageResponse> oldMessageResponse = messageResponseRepository
                .findByIdOrMessage(fieldValidator.getMessageResponse().getId(), fieldValidator.getMessageResponse().getMessage());
        if (oldMessageResponse.isPresent()) {
            return oldMessageResponse.get();
        }
        int code = messageResponseRepository.getMaxCode().orElse(5000);
        newMessageResponse.setCode(code + 1);
        return newMessageResponse;
    }

    private ValidatorValue getValidatorValue(FieldValidator fieldValidator) {
       return validatorValueRepository
                .findByIdOrValue(fieldValidator.getValidatorValue().getId(),
                        fieldValidator.getValidatorValue().getValue())
        .orElse(fieldValidator.getValidatorValue());
    }
}
