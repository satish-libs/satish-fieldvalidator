package com.satish.fieldvalidator.datajpaadapter.repository.adapters;

import com.satish.common.crudop.domainlayer.CRUDAbstract;
import com.satish.common.crudop.repositorylayer.impl.DrivenAdapterRepositoryImpl;
import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.fieldvalidator.datajpaadapter.annotation.aop.trypersist.TryPersist;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageResponse;
import com.satish.fieldvalidator.datajpaadapter.entity.Validator;
import com.satish.fieldvalidator.datajpaadapter.entity.ValidatorValue;
import com.satish.fieldvalidator.datajpaadapter.fascade.FieldMapperFacade;
import com.satish.fieldvalidator.datajpaadapter.fascade.FieldRepositoryFacade;
import com.satish.fieldvalidator.datajpaadapter.repository.FieldValidatorRepository;
import com.satish.fieldvalidator.datajpaadapter.repository.MessageResponseRepository;
import com.satish.fieldvalidator.datajpaadapter.repository.ValidatorRepository;
import com.satish.fieldvalidator.datajpaadapter.repository.ValidatorValueRepository;
import com.satish.fieldvalidator.domain.domain.*;
import com.satish.fieldvalidator.domain.spi.FieldValidatorDrivenPort;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
@Primary
public class FieldValidatorDrivenAdapter extends CRUDAbstract<FieldValidatorDomain, Integer> implements FieldValidatorDrivenPort {

    private CollectionMapper<MessageResponseDomain, MessageResponse> mapMessageResponses;
    private CollectionMapper<ValidatorValueDomain, ValidatorValue> mapValidatorValues;
    private CollectionMapper<ValidatorDomain, Validator> mapValidators;

    private MessageResponseRepository messageResponseRepository;
    private ValidatorRepository validatorRepository;
    private ValidatorValueRepository validatorValueRepository;
    private FieldValidatorRepository fieldValidatorRepository;

    public FieldValidatorDrivenAdapter(final FieldRepositoryFacade fieldRepositoryFacade,
                                       final FieldMapperFacade fieldMapperFacade
    ) {
        super(new DrivenAdapterRepositoryImpl<>(fieldRepositoryFacade.getFieldValidatorRepository(),
                fieldMapperFacade.getFieldValidatorMapper()));

        mapMessageResponses = fieldMapperFacade.mapMessageResponses();
        mapValidators = fieldMapperFacade.mapValidators();
        mapValidatorValues = fieldMapperFacade.mapValidatorValues();

        messageResponseRepository = fieldRepositoryFacade.getMessageResponseRepository();
        validatorRepository = fieldRepositoryFacade.getValidatorRepository();
        validatorValueRepository = fieldRepositoryFacade.getValidatorValueRepository();
        fieldValidatorRepository = fieldRepositoryFacade.getFieldValidatorRepository();
    }

    @TryPersist
    @Override
    public void updateMessageResponseOfField(int id, String newMessage) {
        fieldValidatorRepository.updateMessageResponseOfField(id, newMessage);
    }

    @TryPersist
    @Override
    public void updateValidatorValue(int id, String newValue) {
        fieldValidatorRepository.updateValidatorValueOfField(id, newValue);
    }

    @Override
    public FieldValidatorCollection getFieldValidators() {
        FieldValidatorCollection fieldValidatorCollection = new FieldValidatorCollection();

        fieldValidatorCollection.setMessageResponses((List<MessageResponseDomain>) mapMessageResponses
                .toTargets(messageResponseRepository.findAll()));

        fieldValidatorCollection.setValidators((List<ValidatorDomain>) mapValidators
                .toTargets(validatorRepository.findAll()));

        fieldValidatorCollection.setValidatorValues((List<ValidatorValueDomain>) mapValidatorValues
                .toTargets(validatorValueRepository.findAll()));

        return fieldValidatorCollection;
    }
}
