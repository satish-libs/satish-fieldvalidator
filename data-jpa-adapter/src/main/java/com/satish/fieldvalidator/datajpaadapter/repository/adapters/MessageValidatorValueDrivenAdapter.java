package com.satish.fieldvalidator.datajpaadapter.repository.adapters;

import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.datajpaadapter.entity.MessageValidatorValue;
import com.satish.fieldvalidator.datajpaadapter.mapper.MessageValidatorMapper;
import com.satish.fieldvalidator.datajpaadapter.repository.MessageValidatorValueRepository;
import com.satish.fieldvalidator.domain.domain.MessageValidatorValueDomain;
import com.satish.fieldvalidator.domain.spi.DataBindErrorMessageDrivenPort;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Transactional
@Primary
@RequiredArgsConstructor(onConstructor_ = @Lazy)
public class MessageValidatorValueDrivenAdapter implements DataBindErrorMessageDrivenPort {

    private final MessageValidatorValueRepository messageValidatorValueRepository;
    private final MessageValidatorMapper messageValidatorMapper = Mappers.getMapper(MessageValidatorMapper.class);

    @Override
    public Set<MessageValidatorValueDomain> getMessageResponses(String fieldKey, ValidatorType[] validatorTypes) {
        List<String> validators = new ArrayList<>();
        for (ValidatorType validatorType : validatorTypes) {
            validators.add(validatorType.name());
        }
        Set<MessageValidatorValue> messageValidatorValues = messageValidatorValueRepository.getMessageResponseByFieldNameAndValidators(fieldKey, validators);
        return messageValidatorMapper.toDomains(messageValidatorValues);
    }
}
