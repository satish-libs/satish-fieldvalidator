package com.satish.fieldvalidator.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FieldValidatorDomainApplication {

	public static void main(String[] args) {
		SpringApplication.run(FieldValidatorDomainApplication.class, args);
	}

}
