package com.satish.fieldvalidator.domain.annotation;

import com.satish.fieldvalidator.domain.FieldValidatorDomainApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Import(FieldValidatorDomainApplication.class)
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableFieldValidatorDom {
}
