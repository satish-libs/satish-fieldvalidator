package com.satish.fieldvalidator.domain.api;

import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.domain.base.BaseMessageResponse;

import java.util.Set;

public interface DataBindErrorMessageServicePort {
    Set<BaseMessageResponse> getDataBindErrorMessages(String fieldKey, String value, ValidatorType[] validators);
}
