package com.satish.fieldvalidator.domain.api;

import com.satish.common.crudop.domainlayer.RepositoryPort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.domain.port.FieldPort;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface FieldServicePort extends RepositoryPort<FieldDomain, Integer>, FieldPort {

    List<ValidatorDomain> getValidators();

    FieldDomain addFieldValidator(int id, Set<FieldValidatorDomain> fieldValidators);

    List<ValidatorDomain> remainingValidators(FieldDomain fieldDomain);

    default FieldDomain getFromFieldsById(List<FieldDomain> fields, int id) {
        return fields.stream()
                .filter(fieldDomain -> fieldDomain.getId() == id)
                .findFirst()
                .orElse(null);

    }

    default FieldDomain firstOf(List<FieldDomain> fields) {
        if(fields == null ) {
            return null;
        }
        return fields.get(0);
    }

    default List<ValidatorDomain> mapToValidators(FieldDomain field) {
        if(field == null) {
            return Collections.emptyList();
        }
        return field.getFieldValidators().stream().map(FieldValidatorDomain::getValidator)
                .collect(Collectors.toList());
    }

    default Set<FieldValidatorDomain> getFieldValidators(FieldDomain field) {
        if(field == null) {
            return Collections.emptySet();
        }
        return field.getFieldValidators();
    }

    default FieldValidatorDomain firstFieldValidator(FieldDomain field) {
        if(field == null) {
            return null;
        }
        return field.getFieldValidators().stream().findFirst().orElse(null);
    }
}
