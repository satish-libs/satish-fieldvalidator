package com.satish.fieldvalidator.domain.api;

import com.satish.common.crudop.domainlayer.RepositoryPort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.domain.port.FieldValidatorPort;

import java.util.List;
import java.util.Set;

public interface FieldValidatorServicePort extends RepositoryPort<FieldValidatorDomain, Integer>, FieldValidatorPort {
    List<ValidatorDomain> remainingValidators(int fieldId);
    List<ValidatorDomain> remainingValidators(FieldDomain fieldDomain);
    Set<FieldValidatorDomain> getFieldValidatorsBy(int fieldId);
    default FieldValidatorDomain firstOf(Set<FieldValidatorDomain> fieldValidators) {
        return fieldValidators.stream().findFirst().orElse(null);
    }
}
