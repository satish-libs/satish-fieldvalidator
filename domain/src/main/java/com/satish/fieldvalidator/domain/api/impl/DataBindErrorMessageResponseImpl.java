package com.satish.fieldvalidator.domain.api.impl;

import com.satish.common.util.enums.ValidatorType;
import com.satish.common.util.numberutil.NumberUtil;
import com.satish.common.util.parser.SafeParser;
import com.satish.fieldvalidator.domain.api.DataBindErrorMessageServicePort;
import com.satish.fieldvalidator.domain.domain.MessageValidatorValueDomain;
import com.satish.fieldvalidator.domain.domain.base.BaseMessageResponse;
import com.satish.fieldvalidator.domain.spi.DataBindErrorMessageDrivenPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Primary
public class DataBindErrorMessageResponseImpl implements DataBindErrorMessageServicePort {

    private final DataBindErrorMessageDrivenPort dataBindErrorMessageDrivenPort;

    @Override
    public Set<BaseMessageResponse> getDataBindErrorMessages(String fieldKey, String value, ValidatorType[] validators) {
        Set<MessageValidatorValueDomain> messageValidatorValueDomains = dataBindErrorMessageDrivenPort
                .getMessageResponses(fieldKey, validators);

        return toValidErrorMessageResponse(messageValidatorValueDomains, value);
    }

    private Set<BaseMessageResponse> toValidErrorMessageResponse(Set<MessageValidatorValueDomain> messageValidatorValues, String value) {
        return messageValidatorValues.stream()
                .filter(messageValidatorValue -> isFieldValueInvalid(value, messageValidatorValue))
                .map(BaseMessageResponse.class::cast)
                .collect(Collectors.toSet());
    }

    private boolean isFieldValueInvalid(String fieldValue, MessageValidatorValueDomain messageValidatorValueDomain) {
        String validatorType = messageValidatorValueDomain.getType();
        String validatorValue = messageValidatorValueDomain.getValue();
        if (validatorType.equals(ValidatorType.REQUIRED.name())) {
            return fieldValue == null;
        }

        return checkWhenValuePresent(validatorType, fieldValue, validatorValue);
    }

    private boolean checkWhenValuePresent(String validatorType, String fieldValue, String validatorValue) {
        if(fieldValue == null) {
            return false;
        }
        if (validatorType.equals(ValidatorType.PATTERN.name())) {
            return !fieldValue.matches(validatorValue);
        } else {
            return checkMaxMinLength(validatorType, fieldValue, validatorValue) ||
                    checkMaxMinValue(validatorType, fieldValue, validatorValue);
        }
    }

    private boolean checkMaxMinLength(String validatorType, String fieldValue, String validatorValue) {
        int valueLength = fieldValue.length();
        int validatorValueLength = SafeParser.tryParseInt(validatorValue);
        if (validatorType.equals(ValidatorType.MIN_LENGTH.name())) {
            return valueLength < validatorValueLength;
        } else if (validatorType.equals(ValidatorType.MAX_LENGTH.name())) {
            return valueLength > validatorValueLength;
        }
        return false;
    }

    private boolean checkMaxMinValue(String validatorType, String fieldValue, String validatorValue) {
        if (NumberUtil.isNumeric(fieldValue)) {
            double value = SafeParser.tryParseDouble(fieldValue.trim());
            double validatorValueLength = SafeParser.tryParseDouble(validatorValue);
            if (validatorType.equals(ValidatorType.MIN_VALUE.name())) {
                return value < validatorValueLength;
            } else if (validatorType.equals(ValidatorType.MAX_VALUE.name())) {
                return value > validatorValueLength;
            }
        }
        return false;
    }
}
