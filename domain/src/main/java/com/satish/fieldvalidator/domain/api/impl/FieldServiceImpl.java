package com.satish.fieldvalidator.domain.api.impl;

import com.satish.common.crudop.domainlayer.CRUDAbstract;
import com.satish.fieldvalidator.domain.api.FieldServicePort;
import com.satish.fieldvalidator.domain.api.FieldValidatorServicePort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.domain.spi.FieldDrivenPort;
import com.satish.fieldvalidator.domain.util.fieldvalidatorcheck.EvaluateValidatorValue;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


@Service
@Primary
public class FieldServiceImpl extends CRUDAbstract<FieldDomain, Integer> implements FieldServicePort {
    private final FieldValidatorServicePort fieldValidatorServicePort;
    private final FieldDrivenPort fieldDrivenPort;

    public FieldServiceImpl(final FieldDrivenPort fieldDrivenPort, @Lazy final FieldValidatorServicePort fieldValidatorServicePort) {
        super(fieldDrivenPort);
        this.fieldDrivenPort = fieldDrivenPort;
        this.fieldValidatorServicePort = fieldValidatorServicePort;
    }

    @Override
    public FieldDomain save(FieldDomain t) {
        EvaluateValidatorValue.evaluate(t.getFieldValidators());
        return super.save(t);
    }

    @Override
    public List<ValidatorDomain> getValidators() {
        return fieldValidatorServicePort.getFieldValidators().getValidators();
    }

    @Override
    public FieldDomain addFieldValidator(int id, Set<FieldValidatorDomain> fieldValidators) {
        FieldDomain fieldDomain = findById(id);
        if(!fieldDomain.getFieldValidators().isEmpty()) {
            fieldDomain.getFieldValidators().addAll(fieldValidators);
        } else {
            fieldDomain.setFieldValidators(fieldValidators);
        }
        EvaluateValidatorValue.evaluate(fieldValidators);
        return super.save(fieldDomain);
    }

    @Override
    public List<ValidatorDomain> remainingValidators(FieldDomain fieldDomain) {
        return fieldValidatorServicePort.remainingValidators(fieldDomain);
    }

    @Override
    public List<FieldDomain> getActiveFields() {
        return fieldDrivenPort.getActiveFields();
    }
}
