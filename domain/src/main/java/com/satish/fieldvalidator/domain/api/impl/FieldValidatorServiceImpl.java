package com.satish.fieldvalidator.domain.api.impl;

import com.satish.common.crudop.domainlayer.CRUDAbstract;
import com.satish.fieldvalidator.domain.api.FieldServicePort;
import com.satish.fieldvalidator.domain.api.FieldValidatorServicePort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorCollection;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.domain.spi.FieldValidatorDrivenPort;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Primary
public class FieldValidatorServiceImpl extends CRUDAbstract<FieldValidatorDomain, Integer> implements FieldValidatorServicePort {
    private final FieldValidatorDrivenPort fieldValidatorDrivenPort;
    private final FieldServicePort fieldServicePort;

    public FieldValidatorServiceImpl(final FieldValidatorDrivenPort fieldValidatorDrivenPort, final FieldServicePort fieldServicePort) {
        super(fieldValidatorDrivenPort);
        this.fieldValidatorDrivenPort = fieldValidatorDrivenPort;
        this.fieldServicePort = fieldServicePort;
    }

    @Override
    public void updateMessageResponseOfField(int id, String newMessage) {
        this.fieldValidatorDrivenPort.updateMessageResponseOfField(id, newMessage);
    }

    @Override
    public void updateValidatorValue(int id, String newValue) {
        this.fieldValidatorDrivenPort.updateValidatorValue(id, newValue);
    }

    @Override
    public FieldValidatorCollection getFieldValidators() {
        return this.fieldValidatorDrivenPort.getFieldValidators();
    }

    @Override
    public List<ValidatorDomain> remainingValidators(int fieldId) {
        return remainingValidators(fieldServicePort.findById(fieldId));
    }

    @Override
    public List<ValidatorDomain> remainingValidators(FieldDomain fieldDomain) {
        List<ValidatorDomain> addedValidator = fieldServicePort.mapToValidators(fieldDomain);
        List<ValidatorDomain> validators = getFieldValidators().getValidators();
        return validators.stream()
                .filter(validator -> !addedValidator.contains(validator))
                .collect(Collectors.toList());
    }

    @Override
    public Set<FieldValidatorDomain> getFieldValidatorsBy(int fieldId) {
        return fieldServicePort.findById(fieldId)
                .getFieldValidators();
    }
}
