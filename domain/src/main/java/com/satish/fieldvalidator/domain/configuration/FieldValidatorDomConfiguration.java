package com.satish.fieldvalidator.domain.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.satish.fieldvalidator.domain")
public class FieldValidatorDomConfiguration {
}
