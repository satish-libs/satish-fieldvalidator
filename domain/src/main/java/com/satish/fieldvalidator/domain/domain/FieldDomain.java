package com.satish.fieldvalidator.domain.domain;

import com.satish.fieldvalidator.domain.domain.base.Identity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FieldDomain extends Identity {

    private String fieldKey;

    private String name;

    private String placeholder;

    private String label;

    private boolean status;

    private Set<FieldValidatorDomain> fieldValidators = new HashSet<>();

    public FieldDomain(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldDomain fieldDomain = (FieldDomain) o;
        return Objects.equals(getId(), fieldDomain.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
