package com.satish.fieldvalidator.domain.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FieldValidatorCollection {

    private List<MessageResponseDomain> messageResponses = new ArrayList<>();

    private List<ValidatorDomain> validators = new ArrayList<>();

    private List<ValidatorValueDomain> validatorValues = new ArrayList<>();
}
