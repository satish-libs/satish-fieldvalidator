package com.satish.fieldvalidator.domain.domain;

import com.satish.fieldvalidator.domain.domain.base.Identity;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class FieldValidatorDomain extends Identity {

    private FieldDomain field;

    private MessageResponseDomain messageResponse;

    private ValidatorDomain validator;

    private ValidatorValueDomain validatorValue;

    private boolean status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FieldValidatorDomain that = (FieldValidatorDomain) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(messageResponse, that.messageResponse) &&
                Objects.equals(validator, that.validator) &&
                Objects.equals(validatorValue, that.validatorValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), field, messageResponse, validator, validatorValue);
    }
}
