package com.satish.fieldvalidator.domain.domain;

import com.satish.fieldvalidator.domain.domain.base.BaseMessageResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class MessageResponseDomain extends BaseMessageResponse {

    public MessageResponseDomain(int id, int code, String message) {
        super(id, code, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageResponseDomain messageResponse = (MessageResponseDomain) o;
        return Objects.equals(getCode(), messageResponse.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }
}
