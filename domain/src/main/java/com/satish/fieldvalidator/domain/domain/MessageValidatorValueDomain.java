package com.satish.fieldvalidator.domain.domain;

import com.satish.fieldvalidator.domain.domain.base.BaseMessageResponse;

import java.util.Objects;

public class MessageValidatorValueDomain extends BaseMessageResponse {
    private String type;
    private String value;

    public MessageValidatorValueDomain() {}

    public MessageValidatorValueDomain(int id, int code, String message) {
        super(id, code, message);
    }

    public MessageValidatorValueDomain(int id, int code, String message, String type, String value) {
        this(id, code, message);
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageValidatorValueDomain that = (MessageValidatorValueDomain) o;
        return Objects.equals(getCode(), that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    @Override
    public String toString() {
        return "MessageValidatorValueDomain{" +
                "type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
