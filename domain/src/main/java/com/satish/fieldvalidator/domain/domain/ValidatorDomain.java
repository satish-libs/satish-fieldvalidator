package com.satish.fieldvalidator.domain.domain;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.domain.base.Identity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class ValidatorDomain extends Identity {

    private ValidatorType type;

    private ValidatorDataType dataType;

    public ValidatorDomain(int id, ValidatorType type, ValidatorDataType dataType) {
        this.setId(id);
        this.type = type;
        this.dataType = dataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ValidatorDomain validator = (ValidatorDomain) o;
        return Objects.equals(getId(), validator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
