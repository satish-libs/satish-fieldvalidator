package com.satish.fieldvalidator.domain.domain;

import com.satish.fieldvalidator.domain.domain.base.Identity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class ValidatorValueDomain extends Identity {

    private String value;

    public ValidatorValueDomain(int id, String value) {
        this.setId(id);
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorValueDomain validator = (ValidatorValueDomain) o;
        return Objects.equals(getId(), validator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
