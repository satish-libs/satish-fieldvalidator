package com.satish.fieldvalidator.domain.domain.base;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BaseDate extends Identity {
    private LocalDateTime createdDateTime;
    private LocalDateTime updateDateTime;
}
