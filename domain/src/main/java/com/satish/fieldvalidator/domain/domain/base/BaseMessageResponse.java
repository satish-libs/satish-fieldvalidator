package com.satish.fieldvalidator.domain.domain.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseMessageResponse {

    private int id;

    private int code;

    private String message;
}
