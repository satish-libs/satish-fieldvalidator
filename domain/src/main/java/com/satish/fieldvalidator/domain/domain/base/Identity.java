package com.satish.fieldvalidator.domain.domain.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Identity {
    private int id;
}
