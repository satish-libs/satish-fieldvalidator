package com.satish.fieldvalidator.domain.exceptions;

public class DataConflictException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 3354089972707272699L;

    public DataConflictException(String message) {
        super(message);
    }
}
