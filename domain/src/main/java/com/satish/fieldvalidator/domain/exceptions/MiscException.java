package com.satish.fieldvalidator.domain.exceptions;

public class MiscException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 5812978243574913285L;

    public MiscException(String message) {
        super(message);
    }
}
