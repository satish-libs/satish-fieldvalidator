package com.satish.fieldvalidator.domain.exceptions;

public class NoDataException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -3376041004083099919L;

    public NoDataException(String messageId) {
        super(messageId);
    }
}
