package com.satish.fieldvalidator.domain.port;

import com.satish.fieldvalidator.domain.domain.FieldDomain;

import java.util.List;

public interface FieldPort {
    List<FieldDomain> getActiveFields();
}
