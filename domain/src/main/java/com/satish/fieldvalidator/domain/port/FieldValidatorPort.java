package com.satish.fieldvalidator.domain.port;

import com.satish.fieldvalidator.domain.domain.FieldValidatorCollection;

public interface FieldValidatorPort {
    void updateMessageResponseOfField(int id, String newMessage);
    void updateValidatorValue(int id, String newValue);
    FieldValidatorCollection getFieldValidators();
}
