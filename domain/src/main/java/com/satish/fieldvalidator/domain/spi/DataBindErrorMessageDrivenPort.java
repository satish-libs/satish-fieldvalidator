package com.satish.fieldvalidator.domain.spi;

import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.domain.MessageValidatorValueDomain;

import java.util.Set;

public interface DataBindErrorMessageDrivenPort {
    Set<MessageValidatorValueDomain> getMessageResponses(String fieldKey, ValidatorType[] validatorTypes);

}
