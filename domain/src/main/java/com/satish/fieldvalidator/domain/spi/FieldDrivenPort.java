package com.satish.fieldvalidator.domain.spi;

import com.satish.common.crudop.domainlayer.RepositoryPort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.port.FieldPort;

public interface FieldDrivenPort extends RepositoryPort<FieldDomain, Integer>, FieldPort {
}
