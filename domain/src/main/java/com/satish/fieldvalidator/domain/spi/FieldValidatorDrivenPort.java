package com.satish.fieldvalidator.domain.spi;

import com.satish.common.crudop.domainlayer.RepositoryPort;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.port.FieldValidatorPort;

public interface FieldValidatorDrivenPort extends FieldValidatorPort, RepositoryPort<FieldValidatorDomain, Integer> {
}
