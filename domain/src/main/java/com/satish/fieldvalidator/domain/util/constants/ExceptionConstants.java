package com.satish.fieldvalidator.domain.util.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionConstants {
    public static String NO_VALIDATOR_VALUE = "Validator value not found in the database table";
}
