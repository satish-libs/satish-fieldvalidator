package com.satish.fieldvalidator.domain.util.fieldvalidatorcheck;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorValueDomain;

import java.util.Set;

import static com.satish.common.util.parser.SafeParser.tryParseDouble;

public final class EvaluateValidatorValue {

    private static final String EXCEPTION_MESSAGE = "Validator value must be of type ";

    private EvaluateValidatorValue() {
    }

    public static void evaluate(Set<FieldValidatorDomain> fieldValidatorDomains) {
        fieldValidatorDomains.forEach(EvaluateValidatorValue::evaluate);
    }

    public static void evaluate(final FieldValidatorDomain fieldValidatorDomain) {
        setWhenBooleanType(fieldValidatorDomain);
        tryParseNumber(fieldValidatorDomain.getValidator().getDataType(), fieldValidatorDomain.getValidatorValue().getValue());
    }

    private static void setWhenBooleanType(final FieldValidatorDomain fieldValidatorDomain) {
        if (fieldValidatorDomain.getValidator().getDataType().equals(ValidatorDataType.BOOLEAN)) {
            fieldValidatorDomain.setValidatorValue(new ValidatorValueDomain(0, "true"));
        }
    }

    private static void tryParseNumber(ValidatorDataType validatorDataType, String validatorValue) {
        if (validatorDataType.equals(ValidatorDataType.INTEGER) || validatorDataType.equals(ValidatorDataType.DECIMAL)) {
            tryParseDouble(validatorValue,
                    EXCEPTION_MESSAGE + validatorDataType.name().toLowerCase());
        }
    }

}
