package com.satish.fieldvalidator.webcore.configuration;

import com.satish.fieldvalidator.webcore.dto.Error;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashSet;
import java.util.Set;

@Configuration
@ComponentScan(basePackages ="com.satish.fieldvalidator.webcore")
public class ErrorConfiguration {
    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Set<Error> getErrors() {
        return new HashSet<>();
    }
}
