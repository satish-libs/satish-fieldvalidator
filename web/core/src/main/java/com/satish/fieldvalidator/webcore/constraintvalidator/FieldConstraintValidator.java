package com.satish.fieldvalidator.webcore.constraintvalidator;

import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.api.DataBindErrorMessageServicePort;
import com.satish.fieldvalidator.webcore.dto.Error;
import com.satish.fieldvalidator.webcore.dto.base.HttpMessageResponse;
import com.satish.fieldvalidator.webcore.mapper.HTTPMessageResponseMapper;
import com.satish.fieldvalidator.webcore.util.constants.NumberConstants;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

@RequiredArgsConstructor
public class FieldConstraintValidator implements ConstraintValidator<FieldValidator, Object> {

    private final DataBindErrorMessageServicePort dataBindErrorMessageServicePort;

    private final Set<Error> errors;

    private String fieldKey;
    private ValidatorType[] validatorTypes;
    private HTTPMessageResponseMapper httpMessageResponseMapper;


    @Override
    public void initialize(FieldValidator constraintAnnotation) {
        fieldKey = constraintAnnotation.fieldKey();
        validatorTypes = constraintAnnotation.fieldValidators();
        httpMessageResponseMapper = Mappers.getMapper(HTTPMessageResponseMapper.class);
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String parsedValue = value != null ? value.toString() : null;
        Set<HttpMessageResponse> errorMessages = httpMessageResponseMapper
                .toHttpMessageResponse(dataBindErrorMessageServicePort
                        .getDataBindErrorMessages(fieldKey, parsedValue, validatorTypes));
        if (errorMessages.size() > NumberConstants.EMPTY_LIST) {
            errors.add(new Error(fieldKey, value, errorMessages));
        }
        return errorMessages.isEmpty();
    }
}
