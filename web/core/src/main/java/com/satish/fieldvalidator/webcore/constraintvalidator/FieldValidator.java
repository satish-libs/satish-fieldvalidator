package com.satish.fieldvalidator.webcore.constraintvalidator;

import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.webcore.util.constants.ErrorMessageConstants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = FieldConstraintValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldValidator {
    String fieldKey();

    String attributeName() default  "";

    ValidatorType[] fieldValidators() default {ValidatorType.MAX_VALUE, ValidatorType.MIN_VALUE, ValidatorType.MAX_LENGTH,
            ValidatorType.MIN_LENGTH, ValidatorType.PATTERN, ValidatorType.REQUIRED};

    String message() default ErrorMessageConstants.INVALID_PARAMETER;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
