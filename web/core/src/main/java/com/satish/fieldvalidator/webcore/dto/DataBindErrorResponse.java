package com.satish.fieldvalidator.webcore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class DataBindErrorResponse extends HttpErrorResponse {
    private Set<Error> errors;

    public DataBindErrorResponse(int code, String message, HttpStatus status, Set<Error> errors) {
        super(code, message, status);
        this.errors = errors;
    }
}
