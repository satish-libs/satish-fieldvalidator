package com.satish.fieldvalidator.webcore.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.satish.fieldvalidator.webcore.constraintvalidator.FieldValidator;
import com.satish.fieldvalidator.webcore.dto.base.Identity;
import com.satish.fieldvalidator.webcore.util.constants.FieldKeyConstant.Field;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldDTO extends Identity {

    @NotBlank(message = "Field key is a required field")
    @Size(min = 2, max = 30, message = "Field key must be between 2 and 30 characters")
    private String fieldKey;

    @NotBlank(message = "Field name is a required field")
    @Size(min = 2, max = 30, message = "Field name must be between 2 and 30 characters")
    private String name;

    @NotBlank(message = "Field label is a required field")
    @Size(min = 2, max = 255, message = "Field label must be between 2 and 255 characters")
    private String label;

    @Size(min = 5, max = 255, message = "Placeholder must be between 5 and 255 characters")
    private String placeholder;

    private boolean status;

    @Valid
    @JsonIgnoreProperties("field")
    private Set<FieldValidatorDTO> fieldValidators;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        FieldDTO fieldDTO = (FieldDTO) o;
        return Objects.equals(getId(), fieldDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
