package com.satish.fieldvalidator.webcore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.satish.fieldvalidator.webcore.dto.base.Identity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldValidatorDTO extends Identity {

    private FieldDTO field;

    @Valid
    @NotNull
    private MessageResponseDTO messageResponse;

    @Valid
    @NotNull
    private ValidatorDTO validator;

    @Valid
    @NotNull
    private ValidatorValueDTO validatorValue;

    private boolean status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FieldValidatorDTO that = (FieldValidatorDTO) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(messageResponse, that.messageResponse) &&
                Objects.equals(validator, that.validator) &&
                Objects.equals(validatorValue, that.validatorValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), field, messageResponse, validator, validatorValue);
    }
}
