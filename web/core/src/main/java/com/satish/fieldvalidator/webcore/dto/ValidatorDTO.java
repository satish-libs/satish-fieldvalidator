package com.satish.fieldvalidator.webcore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.satish.fieldvalidator.webcore.dto.base.Identity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidatorDTO extends Identity {

    @NotNull
    private String type;

    @NotNull
    private String dataType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorDTO validator = (ValidatorDTO) o;
        return Objects.equals(getId(), validator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
