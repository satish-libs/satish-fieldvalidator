package com.satish.fieldvalidator.webcore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.satish.fieldvalidator.webcore.dto.base.Identity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidatorValueDTO extends Identity {

    @NotBlank(message = "Validator value is a required field")
    @Size(max = 750, message = "Validator value cannot be more than 750 characters")
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorValueDTO validator = (ValidatorValueDTO) o;
        return Objects.equals(getId(), validator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
