package com.satish.fieldvalidator.webcore.dto.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDate extends Identity {
    private String createdDate;
    private String updatedDate;
}
