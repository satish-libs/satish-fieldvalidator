package com.satish.fieldvalidator.webcore.dto.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpMessageResponse extends Identity {

    private int code;

    @NotBlank(message = "Message response is a required field")
    @Size(max = 255, message = "Message response cannot be more than 255 characters")
    private String message;
}
