package com.satish.fieldvalidator.webcore.dto.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Identity {
    private int id;
}
