package com.satish.fieldvalidator.webcore.exception;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.domain.exceptions.MiscException;
import com.satish.fieldvalidator.domain.exceptions.NoDataException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

public interface CoreException<R> {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    default R methodArgumentException(MethodArgumentNotValidException ex) {
        return null;
    }

    @ExceptionHandler(NoDataException.class)
    default R dataNotFoundException(final NoDataException ex) {
        return null;
    }

    @ExceptionHandler(DataConflictException.class)
    default R dataConflictException(final DataConflictException ex) {
        return null;
    }

    @ExceptionHandler(Exception.class)
    default R exception(final Exception ex, HttpServletRequest request) {
        return null;
    }

    @ExceptionHandler(MiscException.class)
    default R miscException(final MiscException ex, HttpServletRequest request) {
        return null;
    }
}
