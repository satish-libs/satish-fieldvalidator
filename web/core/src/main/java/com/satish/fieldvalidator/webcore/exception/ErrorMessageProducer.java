package com.satish.fieldvalidator.webcore.exception;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.domain.exceptions.MiscException;
import com.satish.fieldvalidator.domain.exceptions.NoDataException;
import com.satish.fieldvalidator.webcore.dto.DataBindErrorResponse;
import com.satish.fieldvalidator.webcore.dto.Error;
import com.satish.fieldvalidator.webcore.dto.HttpErrorResponse;
import com.satish.fieldvalidator.webcore.dto.base.HttpMessageResponse;
import com.satish.fieldvalidator.webcore.util.constants.ConstraintName;
import com.satish.fieldvalidator.webcore.util.constants.ErrorMessageConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.satish.fieldvalidator.webcore.util.parser.ExceptionMessageParser.getFieldName;

@Component
@RequiredArgsConstructor
public class ErrorMessageProducer implements ErrorMessageSpec {

    private final Set<Error> errors;

    @Override
    public HttpErrorResponse dataBind(MethodArgumentNotValidException ex) {
        Set<Error> httpError = errors;
        final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        if (httpError.isEmpty()) {
            httpError = mapToDataBind(ex.getBindingResult());
        }
        ex.printStackTrace();
        return new DataBindErrorResponse(badRequest.value(),
                ErrorMessageConstants.INVALID_PARAMETER, badRequest, httpError);
    }

    @Override
    public HttpErrorResponse noData(NoDataException ex) {
        final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ex.printStackTrace();
        return new HttpErrorResponse(badRequest.value(), ex.getMessage(), badRequest);
    }

    @Override
    public HttpErrorResponse dataConflict(DataConflictException ex) {
        String message = getFieldName(ex.getMessage(), ConstraintName.DUPLICATE, 0, 2) + ErrorMessageConstants.NOT_UNIQUE;
        final HttpStatus conflict = HttpStatus.CONFLICT;
        ex.printStackTrace();
        return new HttpErrorResponse(conflict.value(), message, conflict);
    }

    @Override
    public HttpErrorResponse exception(Exception ex, HttpServletRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ex.printStackTrace();
        return new HttpErrorResponse(status.value(), ex.getMessage(), status);
    }

    @Override
    public HttpErrorResponse miscException(MiscException ex, HttpServletRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ex.printStackTrace();
        return new HttpErrorResponse(status.value(), ex.getMessage(), status);
    }


    private Set<Error> mapToDataBind(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream()
                .map(fieldError -> {
                    Set<HttpMessageResponse> errorMessages = new HashSet<>();
                    errorMessages.add(new HttpMessageResponse(HttpStatus.BAD_REQUEST.value(), fieldError.getDefaultMessage()));
                    return new Error(fieldError.getField(), fieldError.getRejectedValue(), errorMessages);
                }).collect(Collectors.toSet());
    }
}
