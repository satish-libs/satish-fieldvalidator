package com.satish.fieldvalidator.webcore.exception;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.domain.exceptions.MiscException;
import com.satish.fieldvalidator.domain.exceptions.NoDataException;
import com.satish.fieldvalidator.webcore.dto.HttpErrorResponse;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;

public interface ErrorMessageSpec {
    HttpErrorResponse dataBind(MethodArgumentNotValidException ex);

    HttpErrorResponse noData(final NoDataException ex);

    HttpErrorResponse dataConflict(final DataConflictException ex);

    HttpErrorResponse exception(final Exception ex, HttpServletRequest request);

    HttpErrorResponse miscException(final MiscException ex, HttpServletRequest request);
}
