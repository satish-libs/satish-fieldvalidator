package com.satish.fieldvalidator.webcore.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.webcore.dto.FieldDTO;


public interface FieldMapper extends AbstractMapper<FieldDTO, FieldDomain> {
}
