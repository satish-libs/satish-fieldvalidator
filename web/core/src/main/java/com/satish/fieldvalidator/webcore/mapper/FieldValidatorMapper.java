package com.satish.fieldvalidator.webcore.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;

public interface FieldValidatorMapper extends AbstractMapper<FieldValidatorDTO, FieldValidatorDomain> {
}
