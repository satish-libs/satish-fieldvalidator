package com.satish.fieldvalidator.webcore.mapper;

import com.satish.fieldvalidator.domain.domain.base.BaseMessageResponse;
import com.satish.fieldvalidator.webcore.dto.base.HttpMessageResponse;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface HTTPMessageResponseMapper {
    Set<HttpMessageResponse> toHttpMessageResponse(Set<BaseMessageResponse> baseMessageResponses);
}
