package com.satish.fieldvalidator.webcore.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.domain.domain.MessageResponseDomain;
import com.satish.fieldvalidator.webcore.dto.MessageResponseDTO;

public interface MessageResponseMapper extends AbstractMapper<MessageResponseDTO, MessageResponseDomain> {
}
