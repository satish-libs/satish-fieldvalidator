package com.satish.fieldvalidator.webcore.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.webcore.dto.ValidatorDTO;

public interface ValidatorMapper extends AbstractMapper<ValidatorDTO, ValidatorDomain> {
}
