package com.satish.fieldvalidator.webcore.mapper;

import com.satish.common.mapper.generics.AbstractMapper;
import com.satish.fieldvalidator.domain.domain.ValidatorValueDomain;
import com.satish.fieldvalidator.webcore.dto.ValidatorValueDTO;

public interface ValidatorValueMapper extends AbstractMapper<ValidatorValueDTO, ValidatorValueDomain> {
}
