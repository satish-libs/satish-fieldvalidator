package com.satish.fieldvalidator.webcore.mapper.impl;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.concrete.CollectionMapperFactory;
import com.satish.common.util.CollectionKey;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.webcore.dto.FieldDTO;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldMapper;
import com.satish.fieldvalidator.webcore.mapper.FieldValidatorMapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("FieldMapperPA")
public class FieldMapperImpl implements FieldMapper {
    private final CollectionMapper<FieldValidatorDTO, FieldValidatorDomain> collectionMapper;

    public FieldMapperImpl(final FieldValidatorMapper fieldValidatorMapper) {
        collectionMapper = CollectionMapperFactory.getMapper(CollectionKey.SET, fieldValidatorMapper);
    }

    @Override
    public FieldDTO toTarget(FieldDomain source) {
        if (source == null) {
            return null;
        }
        FieldDTO fieldDTO = new FieldDTO();
        fieldDTO.setFieldKey(source.getFieldKey());
        fieldDTO.setLabel(source.getLabel());
        fieldDTO.setName(source.getName());
        fieldDTO.setPlaceholder(source.getPlaceholder());
        fieldDTO.setId(source.getId());
        fieldDTO.setStatus(source.isStatus());
        fieldDTO.setFieldValidators((Set<FieldValidatorDTO>) collectionMapper.toTargets(source.getFieldValidators()));
        return fieldDTO;
    }

    @Override
    public FieldDomain toSource(FieldDTO target) {
        if (target == null) {
            return null;
        }
        FieldDomain field = new FieldDomain();
        field.setFieldKey(target.getFieldKey());
        field.setLabel(target.getLabel());
        field.setName(target.getName());
        field.setPlaceholder(target.getPlaceholder());
        field.setId(target.getId());
        field.setStatus(target.isStatus());
        Set<FieldValidatorDomain> fieldValidatorDomains = (Set<FieldValidatorDomain>) collectionMapper.toSources(target.getFieldValidators());
        field.setFieldValidators(fieldValidatorDomains);
        return field;
    }
}
