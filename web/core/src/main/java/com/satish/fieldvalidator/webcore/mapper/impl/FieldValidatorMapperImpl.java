package com.satish.fieldvalidator.webcore.mapper.impl;

import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldValidatorMapper;
import com.satish.fieldvalidator.webcore.mapper.MessageResponseMapper;
import com.satish.fieldvalidator.webcore.mapper.ValidatorMapper;
import com.satish.fieldvalidator.webcore.mapper.ValidatorValueMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service("webFieldValidatorMapper")
@RequiredArgsConstructor
public class FieldValidatorMapperImpl implements FieldValidatorMapper {
    private final MessageResponseMapper messageResponseMapper;
    private final ValidatorMapper validatorMapper;
    private final ValidatorValueMapper validatorValueMapper;

    @Override
    public FieldValidatorDTO toTarget(FieldValidatorDomain source) {
        if(source == null) {
            return null;
        }
        FieldValidatorDTO fieldValidatorDTO = new FieldValidatorDTO();
        fieldValidatorDTO.setId(source.getId());
        fieldValidatorDTO.setMessageResponse(messageResponseMapper.toTarget(source.getMessageResponse()));
        fieldValidatorDTO.setStatus(source.isStatus());
        fieldValidatorDTO.setValidator(validatorMapper.toTarget(source.getValidator()));
        fieldValidatorDTO.setValidatorValue(validatorValueMapper.toTarget(source.getValidatorValue()));
        return fieldValidatorDTO;
    }

    @Override
    public FieldValidatorDomain toSource(FieldValidatorDTO target) {
        if(target == null) {
            return null;
        }
        FieldValidatorDomain fieldValidatorDomain = new FieldValidatorDomain();
        fieldValidatorDomain.setId(target.getId());
        fieldValidatorDomain.setMessageResponse(messageResponseMapper.toSource(target.getMessageResponse()));
        fieldValidatorDomain.setStatus(target.isStatus());
        fieldValidatorDomain.setValidator(validatorMapper.toSource(target.getValidator()));
        fieldValidatorDomain.setValidatorValue(validatorValueMapper.toSource(target.getValidatorValue()));
        return fieldValidatorDomain;
    }
}
