package com.satish.fieldvalidator.webcore.mapper.impl;

import com.satish.fieldvalidator.domain.domain.MessageResponseDomain;
import com.satish.fieldvalidator.webcore.dto.MessageResponseDTO;
import com.satish.fieldvalidator.webcore.mapper.MessageResponseMapper;
import org.springframework.stereotype.Service;

@Service("MessageResponseMapperPA")
public class MessageResponseMapperImpl implements MessageResponseMapper {
    @Override
    public MessageResponseDTO toTarget(MessageResponseDomain source) {
        if (source == null) {
            return null;
        }
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO();
        messageResponseDTO.setId(source.getId());
        messageResponseDTO.setCode(source.getCode());
        messageResponseDTO.setMessage(source.getMessage());
        return messageResponseDTO;
    }

    @Override
    public MessageResponseDomain toSource(MessageResponseDTO target) {
        if (target == null) {
            return null;
        }
        MessageResponseDomain messageResponseDomain = new MessageResponseDomain();
        messageResponseDomain.setId(target.getId());
        messageResponseDomain.setMessage(target.getMessage());
        messageResponseDomain.setCode(target.getCode());
        return  messageResponseDomain;
    }
}
