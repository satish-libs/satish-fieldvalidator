package com.satish.fieldvalidator.webcore.mapper.impl;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.webcore.dto.ValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.ValidatorMapper;
import org.springframework.stereotype.Service;

@Service("ValidatorMapperPA")
public class ValidatorMapperImpl implements ValidatorMapper {
    @Override
    public ValidatorDTO toTarget(ValidatorDomain source) {
        if(source == null) {
            return null;
        }
        ValidatorDTO validatorDTO = new ValidatorDTO();
        validatorDTO.setId(source.getId());
        validatorDTO.setDataType(source.getDataType().name());
        validatorDTO.setType(source.getType().name());
        return validatorDTO;
    }

    @Override
    public ValidatorDomain toSource(ValidatorDTO target) {
        if(target == null) {
            return null;
        }
        ValidatorDomain validatorDomain = new ValidatorDomain();
        validatorDomain.setDataType(ValidatorDataType.valueOf(target.getDataType()));
        validatorDomain.setId(target.getId());
        validatorDomain.setType(ValidatorType.valueOf(target.getType()));
        return validatorDomain;
    }
}
