package com.satish.fieldvalidator.webcore.mapper.impl;

import com.satish.fieldvalidator.domain.domain.ValidatorValueDomain;
import com.satish.fieldvalidator.webcore.dto.ValidatorValueDTO;
import com.satish.fieldvalidator.webcore.mapper.ValidatorValueMapper;
import org.springframework.stereotype.Service;

@Service("ValidatorValueMapperPA")
public class ValidatorValueMapperImpl implements ValidatorValueMapper {
    @Override
    public ValidatorValueDTO toTarget(ValidatorValueDomain source) {
        if(source == null) {
            return null;
        }
        ValidatorValueDTO validatorValueDTO = new ValidatorValueDTO();
        validatorValueDTO.setValue(source.getValue());
        validatorValueDTO.setId(source.getId());
        return validatorValueDTO;
    }

    @Override
    public ValidatorValueDomain toSource(ValidatorValueDTO target) {
        if(target == null) {
            return null;
        }
        ValidatorValueDomain validatorValueDomain = new ValidatorValueDomain();
        validatorValueDomain.setValue(target.getValue());
        validatorValueDomain.setId(target.getId());
        return validatorValueDomain;
    }
}
