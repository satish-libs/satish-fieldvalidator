package com.satish.fieldvalidator.webcore.util.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstraintName {
    public final static String DUPLICATE = "Duplicate";
}
