package com.satish.fieldvalidator.webcore.util.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorMessageConstants {

    public final static String INVALID_PARAMETER = "Invalid Request Parameter";
    public final static String NOT_UNIQUE = " has already been added";
}
