package com.satish.fieldvalidator.webcore.util.constants;

public final class FieldKeyConstant {

    private FieldKeyConstant() {}

    public static class Field {
        private Field() {}
        public static final String FIELD_KEY = "fieldKey";
        public static final String NAME = "name";
        public static final String LABEL = "label";
        public static final String PLACEHOLDER = "placeholder";
    }

    public static class MessageResponse {
        private MessageResponse() {}
        public static final String MESSAGE = "messageResponse";
    }

    public static class  ValidatorValue {
        private ValidatorValue() {}
        public static final String VALUE = "validatorValue";
    }
}