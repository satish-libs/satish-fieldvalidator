package com.satish.fieldvalidator.webcore.util.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NumberConstants {
    public final static int EMPTY_LIST = 0;
}
