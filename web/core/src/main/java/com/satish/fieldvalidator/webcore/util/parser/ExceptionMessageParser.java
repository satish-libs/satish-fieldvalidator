package com.satish.fieldvalidator.webcore.util.parser;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessageParser {
    public static String getFieldName(String message, String constraintName, int constraintNameIndex, int fieldNameIndex) {
        if(message != null && !message.isEmpty()) {
            String[] values = message.split(" ");
            if(values[constraintNameIndex].equalsIgnoreCase(constraintName)) {
                return values[fieldNameIndex];
            }
            return "Invalid constraint name";
        }
        return "Message not found";
    }
}
