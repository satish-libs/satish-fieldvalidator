package com.satish.fieldvalidator.mvcadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcAdapterApplication.class, args);
	}

}
