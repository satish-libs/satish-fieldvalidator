package com.satish.fieldvalidator.mvcadapter.annotation;

import com.satish.fieldvalidator.mvcadapter.configuration.DispatcherServletCreator;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Import({DispatcherServletCreator.class})
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableUI {
}
