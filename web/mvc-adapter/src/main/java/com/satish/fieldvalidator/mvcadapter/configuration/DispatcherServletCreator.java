package com.satish.fieldvalidator.mvcadapter.configuration;

import com.satish.fieldvalidator.mvcadapter.MvcAdapterApplication;
import com.satish.fieldvalidator.webcore.WebCoreApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

@Configuration
public class DispatcherServletCreator {

    @Bean
    public ServletRegistrationBean<DispatcherServlet> mvcAdapter() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(MvcAdapterApplication.class, WebCoreApplication.class);
        dispatcherServlet.setApplicationContext(applicationContext);
        ServletRegistrationBean<DispatcherServlet> servletRegistrationBean = new ServletRegistrationBean<>(dispatcherServlet, "/fieldValidatorUI/*");
        servletRegistrationBean.setName("mvcAdapter");
        servletRegistrationBean.setLoadOnStartup(-1);
        return servletRegistrationBean;
    }
}
