package com.satish.fieldvalidator.mvcadapter.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages ="com.satish.fieldvalidator.mvcadapter")
public class FieldValidatorMVCAdapterConfig {

}
