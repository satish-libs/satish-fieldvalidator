package com.satish.fieldvalidator.mvcadapter.controller;

import com.satish.fieldvalidator.domain.api.FieldServicePort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.dto.FieldPatch;
import com.satish.fieldvalidator.mvcadapter.mapper.FieldUIMapper;
import com.satish.fieldvalidator.mvcadapter.mapper.FieldValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.mapper.ValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.mvcadapter.util.ListItemConstant;
import com.satish.fieldvalidator.webcore.dto.FieldDTO;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldMapper;
import com.satish.fieldvalidator.webcore.mapper.FieldValidatorMapper;
import com.satish.fieldvalidator.webcore.mapper.ValidatorMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Set;

import static com.satish.common.util.parser.SafeParser.tryParseInt;

@Controller
@RequiredArgsConstructor
public class MVCFieldController {
    private final FieldServicePort fieldServicePort;
    private final FieldUIMapper fieldUIMapper;
    private final ValidatorMapper validatorMapper;
    private final ValidatorUIMapper validatorUIMapper;
    private final FieldValidatorUIMapper fieldValidatorUIMapper;
    private final FieldMapper fieldMapper;
    private final FieldValidatorMapper fieldValidatorMapper;

    @GetMapping("/fieldValidatorUI")
    public ModelAndView index() {
        List<FieldDomain> fields = fieldServicePort.findAll();
        if(fields.isEmpty()) {
            return newField();
        }
        return fieldModelView(fields, fieldServicePort.firstOf(fields));
    }

    @PostMapping("/fieldValidatorUI")
    public ModelAndView save(@RequestBody FieldDTO fieldDTO) {
        FieldDomain fieldDomain = fieldServicePort.save(fieldMapper.toSource(fieldDTO));
        return fieldModelView(List.of(fieldDomain), fieldDomain);
    }

    @GetMapping("/field-edit-frag/{id}/{articleContentId}")
    public ModelAndView fieldEditFragBy(@PathVariable int id, @PathVariable String articleContentId) {
        return getArticleContentBy(fieldServicePort.findById(id), articleContentId, "actionResultFrag");
    }

    @GetMapping("/field-article-contents/{id}/{articleContentId}")
    public ModelAndView cancelFieldEdit(@PathVariable int id, @PathVariable String articleContentId) {
        return getArticleContentBy(fieldServicePort.findById(id), articleContentId, "contentFragWithAction");
    }

    @GetMapping("/new")
    public ModelAndView newField() {
        List<ValidatorDomain> validators = fieldServicePort.getValidators();
        return new ModelAndView("field-validator-main", "fragment", "newField")
                .addObject("fields", ListItemConstant.defaultListItem())
                .addObject("validators", validatorMapper.toTargetList(validators))
                .addObject("validatorTypes", validatorUIMapper.toListItems(validators));
    }

    @GetMapping("/{id}")
    public ModelAndView getFieldInfoBy(@PathVariable int id) {
        List<FieldDomain> fields = fieldServicePort.findAll();
        if(fields.isEmpty()) {
            return newField();
        }
        return fieldModelView(fields, fieldServicePort.getFromFieldsById(fields, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<FieldDTO> softDelete(@PathVariable final String id) {
        return ResponseEntity.ok(fieldMapper.toTarget(fieldServicePort.softDelete(tryParseInt(id))));
    }

    @GetMapping("/list-items/{id}")
    public ModelAndView currentFieldListItem(@PathVariable int id) {
        FieldDomain fieldDomain = fieldServicePort.findById(id);
        return new ModelAndView("features/fieldvalidator/view/listpane/list-field :: fieldListItem").addObject("item",
                fieldUIMapper.toListItem(fieldDomain));
    }

    @PatchMapping("/{id}/{articleContentId}")
    public ModelAndView partialUpdateBy(@PathVariable final int id, @PathVariable String articleContentId,
            @RequestBody final FieldPatch field) {
        return getArticleContentBy(fieldServicePort.updateColumnBy(id, field.getColumnName(), field.getValue()),
                articleContentId, "contentFragWithAction");
    }

    @PostMapping("/{id}/field-validator")
    public ModelAndView addFieldValidator(@PathVariable int id, @RequestBody Set<FieldValidatorDTO> fieldValidators) {
        FieldDomain fieldDomain = fieldServicePort.addFieldValidator(id,
                fieldValidatorMapper.toSourceSet(fieldValidators));
        List<ValidatorDomain> unAddedValidators = fieldServicePort.remainingValidators(fieldDomain);
        return new ModelAndView("features/fieldvalidator/add/right-pane/validator/add-field-validator :: addValidator")
                .addObject("validators", validatorMapper.toTargetList(unAddedValidators))
                .addObject("validatorTypes", validatorUIMapper.toListItems(unAddedValidators));
    }



    private ModelAndView fieldModelView(List<FieldDomain> fields, FieldDomain field) {
        Set<FieldValidatorDomain> fieldValidators = fieldServicePort.getFieldValidators(field);
        FieldValidatorDomain fieldValidator = fieldServicePort.firstFieldValidator(field);
        return new ModelAndView("field-validator-main", "fields", fieldUIMapper.toListItems(fields))
                .addObject("fragment", "viewMain").addObject("fieldInfo", fieldUIMapper.toArticleContents(field))
                .addObject("validatorTypes", fieldValidatorUIMapper.toListItems(fieldValidators))
                .addObject("validatorInfo", fieldValidatorUIMapper.toArticleContents(fieldValidator))
                .addObject("validators", validatorMapper.toTargetList(fieldServicePort.getValidators()));
    }

    private ModelAndView getArticleContentBy(FieldDomain fieldDomain, String articleContentId, String fragment) {
        ArticleContent articleContent = fieldUIMapper.toArticleContents(fieldDomain).stream()
                .filter(value -> value.getId().equals(articleContentId)).findFirst().orElse(null);
        return new ModelAndView("shared/widgets/article-widgets/article-sub :: " + fragment).addObject("articleContent",
                articleContent);
    }
}
