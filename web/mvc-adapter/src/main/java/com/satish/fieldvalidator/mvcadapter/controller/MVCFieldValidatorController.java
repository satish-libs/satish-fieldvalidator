package com.satish.fieldvalidator.mvcadapter.controller;

import com.satish.fieldvalidator.domain.api.FieldValidatorServicePort;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.dto.ValidatorPropPatch;
import com.satish.fieldvalidator.mvcadapter.mapper.FieldValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.mapper.ValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldValidatorMapper;
import com.satish.fieldvalidator.webcore.mapper.ValidatorMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.satish.common.util.parser.SafeParser.tryParseInt;

@Controller
@RequestMapping("/field-validators")
@RequiredArgsConstructor
public class MVCFieldValidatorController {

    private final FieldValidatorServicePort fieldValidatorServicePort;
    private final FieldValidatorUIMapper fieldValidatorUIMapper;
    private final FieldValidatorMapper fieldValidatorMapper;
    private final ValidatorUIMapper validatorUIMapper;
    private final ValidatorMapper validatorMapper;

    @GetMapping("/{id}")
    public ModelAndView messageWithValidatorValue(@PathVariable String id) {
        FieldValidatorDomain fieldValidatorDomain = fieldValidatorServicePort.findById(tryParseInt(id));
        return new ModelAndView("features/fieldvalidator/view/view-pane/validator/validator.html :: validatorInfo")
                .addAllObjects(validatorInfo(fieldValidatorDomain));

    }

    @GetMapping("/{fieldId}/new")
    public ModelAndView addFieldValidatorFragment(@PathVariable String fieldId) {
        List<ValidatorDomain> unAddedValidators = fieldValidatorServicePort.remainingValidators(tryParseInt(fieldId));
        return new ModelAndView("features/fieldvalidator/add/right-pane/validator/add-field-validator :: addValidator")
                .addObject("validators", validatorMapper.toTargetList(unAddedValidators))
                .addObject("validatorTypes", validatorUIMapper.toListItems(unAddedValidators));
    }

    @GetMapping("/{fieldId}/cancel")
    public ModelAndView cancelNewFieldValidator(@PathVariable String fieldId) {
        Set<FieldValidatorDomain> fieldValidators = fieldValidatorServicePort
                .getFieldValidatorsBy(tryParseInt(fieldId));
        return new ModelAndView("features/fieldvalidator/view/view-pane/validator/validator :: .validator-props")
                .addObject("validatorTypes", fieldValidatorUIMapper.toListItems(fieldValidators))
                .addAllObjects(validatorInfo(fieldValidatorServicePort.firstOf(fieldValidators)));
    }

    @GetMapping("/validator-edit-frag/{id}/{articleContentId}")
    public ModelAndView validatorPropsEditFragBy(@PathVariable int id, @PathVariable String articleContentId) {
        return getArticleContentBy(id, articleContentId, "actionResultFrag");
    }

    @GetMapping("/validator-article-contents/{id}/{articleContentId}")
    public ModelAndView cancelValidatorPropsEdit(@PathVariable int id, @PathVariable String articleContentId) {
        return contentWithFrag(id, articleContentId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<FieldValidatorDTO> softDelete(@PathVariable final String id) {
        return ResponseEntity.ok(fieldValidatorMapper.toTarget(fieldValidatorServicePort.softDelete(tryParseInt(id))));
    }

    @PatchMapping("/{id}/messageResponse")
    public ModelAndView updateMessageResponse(@PathVariable int id , @RequestBody ValidatorPropPatch validatorPropPatch) {
        this.fieldValidatorServicePort.updateMessageResponseOfField(id, validatorPropPatch.getValue());
        return contentWithFrag(id, validatorPropPatch.getArticleContentId());
    }

    @PatchMapping("/{id}/validatorValue")
    public ModelAndView updateValidatorValue(@PathVariable int id, @RequestBody ValidatorPropPatch validatorPropPatch) {
        this.fieldValidatorServicePort.updateValidatorValue(id, validatorPropPatch.getValue());
        return contentWithFrag(id, validatorPropPatch.getArticleContentId());
    }

    private Map<String, List<ArticleContent>> validatorInfo(FieldValidatorDomain fieldValidatorDomain) {
        return Map.of("validatorInfo", fieldValidatorUIMapper.toArticleContents(fieldValidatorDomain));
    }

    private ModelAndView contentWithFrag(int id, String articleContentId) {
        return getArticleContentBy(id, articleContentId, "contentFragWithAction");
    }

    private ModelAndView getArticleContentBy(int id, String articleContentId, String fragment) {
        FieldValidatorDomain fieldValidator = fieldValidatorServicePort.findById(id);
        ArticleContent articleContent = fieldValidatorUIMapper.toArticleContents(fieldValidator).stream()
                .filter(value -> value.getId().equals(articleContentId)).findFirst().orElse(null);
        return new ModelAndView("shared/widgets/article-widgets/article-sub :: " + fragment).addObject("articleContent",
                articleContent);
    }
}
