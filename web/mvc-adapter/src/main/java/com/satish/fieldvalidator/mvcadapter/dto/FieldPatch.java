package com.satish.fieldvalidator.mvcadapter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldPatch {
    private String columnName;
    private String value;
}
