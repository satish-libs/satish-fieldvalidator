package com.satish.fieldvalidator.mvcadapter.dto;

import lombok.Data;

@Data
public class ValidatorPropPatch {
    private String articleContentId;
    private String value;
}
