package com.satish.fieldvalidator.mvcadapter.exception;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.webcore.dto.HttpErrorResponse;
import com.satish.fieldvalidator.webcore.exception.CoreException;
import com.satish.fieldvalidator.webcore.exception.ErrorMessageSpec;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@ControllerAdvice
@RequiredArgsConstructor
public class MVCAdapterExceptionHandler implements CoreException<ModelAndView> {
    private final ErrorMessageSpec errorMessageProducer;
    @Override
    public ModelAndView dataConflictException(DataConflictException ex) {
        HttpErrorResponse errorResponse = errorMessageProducer.dataConflict(ex);
        return new ModelAndView("field-validator-main :: errorMessage",
                Map.of( "message", errorResponse.getMessage()), HttpStatus.CONFLICT);
    }

    @Override
    public ModelAndView exception(Exception ex, HttpServletRequest request) {
        HttpErrorResponse errorResponse = errorMessageProducer.exception(ex, request);
        String message = errorResponse.getMessage() != null ? errorResponse.getMessage() : "Internal Server Errror";
        return new ModelAndView("field-validator-main :: errorMessage",
                Map.of( "message", message), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
