package com.satish.fieldvalidator.mvcadapter.mapper;

import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;

import java.util.List;

public interface FieldUIMapper {
    ListItem toListItem(FieldDomain field);

    List<ListItem> toListItems(List<FieldDomain> fields);

    List<ArticleContent> toArticleContents(FieldDomain field);

    List<ArticleContent> toArticleContents(FieldDomain field, String articleContentId);
}
