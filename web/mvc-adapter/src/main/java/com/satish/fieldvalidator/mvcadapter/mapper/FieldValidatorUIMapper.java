package com.satish.fieldvalidator.mvcadapter.mapper;

import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;

import java.util.List;
import java.util.Set;

public interface FieldValidatorUIMapper {
    List<ArticleContent> toArticleContents(FieldValidatorDomain fieldValidator);
    List<ListItem> toListItems(Set<FieldValidatorDomain> fieldValidators);
}
