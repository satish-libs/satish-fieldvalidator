package com.satish.fieldvalidator.mvcadapter.mapper;

import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;

import java.util.List;

public interface ValidatorUIMapper {
    List<ListItem> toListItems(List<ValidatorDomain> validators);

}
