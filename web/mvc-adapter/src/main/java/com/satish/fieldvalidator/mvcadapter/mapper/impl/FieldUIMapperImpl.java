package com.satish.fieldvalidator.mvcadapter.mapper.impl;

import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.mvcadapter.mapper.FieldUIMapper;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;
import com.satish.fieldvalidator.mvcadapter.uimodel.ToggleListItem;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.satish.fieldvalidator.mvcadapter.util.StringManipulation.nullToHyphen;

@Component
public class FieldUIMapperImpl implements FieldUIMapper {

    @Override
    public List<ListItem> toListItems(List<FieldDomain> fields) {
        if (fields == null) {
            return Collections.emptyList();
        }
        return fields.stream().map(this::toListItem).collect(Collectors.toList());
    }

    @Override
    public List<ArticleContent> toArticleContents(FieldDomain field) {
        return List.of(new ArticleContent("article_field_key", "Field key", field.getFieldKey(), "fieldKey", false),
                new ArticleContent("article_field_name", "Field name", field.getName(), "fieldName", false),
                new ArticleContent("article_field_label","Label", field.getLabel(), "fieldLabel", false),
                new ArticleContent("article_field_placeholder","Placeholder", field.getPlaceholder(), "fieldPlaceholder", false)

        );
    }

    @Override
    public List<ArticleContent> toArticleContents(FieldDomain field, String articleContentId) {
        List<ArticleContent> articleContents = toArticleContents(field);
        articleContents.forEach(articleContent -> {
            if(articleContent.getId().equals(articleContentId)) {
                articleContent.setShowActionResult(true);
            }
        });
        return articleContents;
    }

    @Override
    public ListItem toListItem(FieldDomain fieldDomain) {
        if (fieldDomain == null) {
            return null;
        }
        ToggleListItem listItem = new ToggleListItem();
        listItem.setId(fieldDomain.getId()+"_field_list");
        listItem.setHref("/fieldValidatorUI/" + fieldDomain.getId());
        listItem.setMainInfo(fieldDomain.getName());
        listItem.setChecked(fieldDomain.isStatus());
        String[] subInfos = { "Key: " + nullToHyphen(fieldDomain.getFieldKey()), "Label: " + nullToHyphen(fieldDomain.getLabel()) };
        listItem.setSubInfos(subInfos);
        return listItem;
    }
}
