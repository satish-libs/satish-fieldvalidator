package com.satish.fieldvalidator.mvcadapter.mapper.impl;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.fieldvalidator.domain.domain.FieldValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.mapper.FieldValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.uimodel.ArticleContent;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;
import com.satish.fieldvalidator.mvcadapter.uimodel.ToggleListItem;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.satish.fieldvalidator.mvcadapter.util.StringManipulation.sentenceCaseByReplace;

@Component
public class FieldValidatorUIMapperImpl implements FieldValidatorUIMapper {
    @Override
    public List<ArticleContent> toArticleContents(FieldValidatorDomain fieldValidator) {
        if(fieldValidator == null) {
            return Collections.emptyList();
        }
        ArticleContent messageResponse =  new ArticleContent("article_message_response", "Message response", fieldValidator.getMessageResponse().getMessage(), "messageResponse", false);
        ArticleContent validatorValue = new ArticleContent("article_validator_value","Validator Value", fieldValidator.getValidatorValue().getValue(), "validatorValue" , false);
        if(fieldValidator.getValidator().getDataType() != ValidatorDataType.BOOLEAN) {
            return List.of(messageResponse, validatorValue);
        }
        return List.of(messageResponse);
    }

    @Override
    public List<ListItem> toListItems(Set<FieldValidatorDomain> fieldValidators) {
        return fieldValidators.stream().map(this::toListItem).collect(Collectors.toList());
    }

    private ListItem toListItem(FieldValidatorDomain fieldValidator) {
        if (fieldValidator == null) {
            return null;
        }
        ToggleListItem listItem = new ToggleListItem();
        listItem.setId(fieldValidator.getId() + "_validator_type");
        listItem.setHref("javascript:void(0)");
        listItem.setChecked(fieldValidator.isStatus());
        listItem.setMainInfo(sentenceCaseByReplace(fieldValidator.getValidator().getType().name(), "_"));
        return listItem;
    }
}
