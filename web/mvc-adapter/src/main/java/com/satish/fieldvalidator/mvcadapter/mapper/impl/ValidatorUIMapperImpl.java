package com.satish.fieldvalidator.mvcadapter.mapper.impl;

import com.satish.fieldvalidator.domain.domain.ValidatorDomain;
import com.satish.fieldvalidator.mvcadapter.mapper.ValidatorUIMapper;
import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.satish.fieldvalidator.mvcadapter.util.StringManipulation.sentenceCaseByReplace;

@Component
public class ValidatorUIMapperImpl implements ValidatorUIMapper {
    @Override
    public List<ListItem> toListItems(List<ValidatorDomain> validators) {
        return validators.stream().map(this::toListItem).collect(Collectors.toList());
    }

    private ListItem toListItem(ValidatorDomain validator) {
        if (validator == null) {
            return null;
        }
        ListItem listItem = new ListItem();
        listItem.setId(validator.getId() + "_validator_type");
        listItem.setHref("javascript:void(0)");
        listItem.setMainInfo(sentenceCaseByReplace(validator.getType().name(), "_"));
        return listItem;
    }
}
