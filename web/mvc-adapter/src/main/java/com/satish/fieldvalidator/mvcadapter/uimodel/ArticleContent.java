package com.satish.fieldvalidator.mvcadapter.uimodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleContent {
    private String id;
    private String label;
    private String value;
    private String controlName;
    private boolean showActionResult;
}
