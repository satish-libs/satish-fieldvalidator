package com.satish.fieldvalidator.mvcadapter.uimodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListItem {
    private String id;
    private String mainInfo;
    private String[] subInfos;
    private String href;
}
