package com.satish.fieldvalidator.mvcadapter.uimodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Switch {
    private String id;
    private String value;
    private String text;
    private boolean checked;
}
