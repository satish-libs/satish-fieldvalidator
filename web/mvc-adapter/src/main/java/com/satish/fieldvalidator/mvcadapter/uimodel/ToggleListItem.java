package com.satish.fieldvalidator.mvcadapter.uimodel;

import lombok.Data;

@Data
public class ToggleListItem extends ListItem {
    private boolean checked;
}
