package com.satish.fieldvalidator.mvcadapter.util;

import com.satish.fieldvalidator.mvcadapter.uimodel.ListItem;

import java.util.List;

public class ListItemConstant {
    private ListItemConstant() {
    }

    public static List<ListItem> defaultListItem() {
        return List.of(new ListItem("0", "", new String[] { "Key:", "Label:" }, "javascript:void(0)"));
    }
}
