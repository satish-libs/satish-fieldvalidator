package com.satish.fieldvalidator.mvcadapter.util;

public final class StringManipulation {
    private StringManipulation() {}

    public static String sentenceCaseByReplace(String value, String delimiter) {
        return sentenceCase(value.replace(delimiter, " "));
    }

    public static String sentenceCase(String value) {
        return value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase();
    }

    public static String nullToHyphen(String value) {
        return replaceNullWith(value, "-");
    }

    public static String replaceNullWith(String value, String replacement) {
        if (value == null) {
            return replacement;
        }
        return value;
    }

}
