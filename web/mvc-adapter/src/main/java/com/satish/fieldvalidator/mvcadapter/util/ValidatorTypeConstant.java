package com.satish.fieldvalidator.mvcadapter.util;

import com.satish.common.util.enums.ValidatorDataType;
import com.satish.common.util.enums.ValidatorType;
import com.satish.fieldvalidator.domain.domain.ValidatorDomain;

import java.util.List;

public final class ValidatorTypeConstant {
        private ValidatorTypeConstant() {
        }

        public static List<ValidatorDomain> validatorTypes() {
            return List.of(new ValidatorDomain(1, ValidatorType.MAX_LENGTH, ValidatorDataType.INTEGER), new ValidatorDomain(2, ValidatorType.MIN_LENGTH, ValidatorDataType.INTEGER),
                    new ValidatorDomain(3, ValidatorType.MAX_VALUE, ValidatorDataType.DECIMAL), new ValidatorDomain(4, ValidatorType.MIN_VALUE, ValidatorDataType.DECIMAL),
                    new ValidatorDomain(5, ValidatorType.REQUIRED, ValidatorDataType.BOOLEAN), new ValidatorDomain(6, ValidatorType.PATTERN, ValidatorDataType.STRING));
        }
    }
