package com.satish.fieldvalidator.restadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAdapterModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestAdapterModuleApplication.class, args);
	}

}
