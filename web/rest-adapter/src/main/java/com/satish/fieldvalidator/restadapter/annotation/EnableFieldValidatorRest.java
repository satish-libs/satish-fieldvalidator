package com.satish.fieldvalidator.restadapter.annotation;

import com.satish.fieldvalidator.restadapter.RestAdapterModuleApplication;
import com.satish.fieldvalidator.webcore.WebCoreApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Import({RestAdapterModuleApplication.class, WebCoreApplication.class})
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableFieldValidatorRest {
}
