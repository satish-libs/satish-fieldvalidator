package com.satish.fieldvalidator.restadapter.controller;

import com.satish.fieldvalidator.domain.api.FieldServicePort;
import com.satish.fieldvalidator.domain.domain.FieldDomain;
import com.satish.fieldvalidator.restadapter.util.constants.APIPathConstant;
import com.satish.fieldvalidator.webcore.dto.FieldDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.satish.fieldvalidator.restadapter.util.constants.APIPathConstant.COLUMN_NAME;
import static com.satish.fieldvalidator.restadapter.util.constants.APIPathConstant.ID_PATH;

@RestController
@RequestMapping(APIPathConstant.BASE_PATH + APIPathConstant.FIELD)
@RequiredArgsConstructor
public class FieldController {
    private final FieldServicePort fieldServicePort;
    private final FieldMapper fieldMapper;

    @PostMapping
    public ResponseEntity<FieldDTO> addFieldValidator(@RequestBody @Valid final FieldDTO fieldDTO) {
        final FieldDomain fieldDomain = fieldServicePort.save(fieldMapper.toSource(fieldDTO));
        final FieldDTO newFieldDTO = fieldMapper.toTarget(fieldDomain);
        return new ResponseEntity<>(newFieldDTO, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<FieldDTO>> getAllFieldsWithValidators() {
        return ResponseEntity.ok(fieldMapper.toTargetList(fieldServicePort.findAll()));
    }

    @DeleteMapping(ID_PATH)
    public ResponseEntity<FieldDTO> softDelete(@PathVariable final String id) {
        return ResponseEntity.ok(fieldMapper.toTarget(fieldServicePort.softDelete(Integer.parseInt(id))));
    }

    @PatchMapping(ID_PATH + COLUMN_NAME)
    public ResponseEntity<FieldDTO> updateFieldColumnBy(@PathVariable final String id,
            @PathVariable final String columnName, @RequestParam final String value) {
        return ResponseEntity
                .ok(fieldMapper.toTarget(fieldServicePort.updateColumnBy(Integer.parseInt(id), columnName, value)));
    }
}
