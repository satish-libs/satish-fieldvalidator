package com.satish.fieldvalidator.restadapter.controller;

import com.satish.fieldvalidator.domain.api.FieldValidatorServicePort;
import com.satish.fieldvalidator.domain.domain.FieldValidatorCollection;
import com.satish.fieldvalidator.webcore.constraintvalidator.FieldValidator;
import com.satish.fieldvalidator.webcore.dto.FieldValidatorDTO;
import com.satish.fieldvalidator.webcore.mapper.FieldValidatorMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.satish.fieldvalidator.restadapter.util.constants.APIPathConstant.*;

@RestController
@RequestMapping(BASE_PATH + FIELD_VALIDATOR)
@RequiredArgsConstructor
public class FieldValidatorController {

    private final FieldValidatorServicePort fieldValidatorService;
    private final FieldValidatorMapper fieldValidatorMapper;

    @PatchMapping(ID_PATH + MESSAGE_RESPONSE)
    public void updateMessageResponse(@PathVariable int id,
            @RequestParam @FieldValidator(fieldKey = "messageResponse") String newMessage) {
        this.fieldValidatorService.updateMessageResponseOfField(id, newMessage);
    }

    @PatchMapping(ID_PATH + VALIDATOR_VALUE)
    public void updateValidatorValue(@PathVariable int id, @RequestParam String newValue) {
        this.fieldValidatorService.updateValidatorValue(id, newValue);
    }

    @GetMapping
    public ResponseEntity<FieldValidatorCollection> getAllValidators() {
        return ResponseEntity.ok(fieldValidatorService.getFieldValidators());
    }

    @DeleteMapping(ID_PATH)
    public ResponseEntity<FieldValidatorDTO> softDelete(@PathVariable int id) {
        return ResponseEntity.ok(fieldValidatorMapper.toTarget(fieldValidatorService.softDelete(id)));
    }
}
