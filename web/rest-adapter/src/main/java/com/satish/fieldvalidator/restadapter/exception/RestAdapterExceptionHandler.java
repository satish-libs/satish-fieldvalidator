package com.satish.fieldvalidator.restadapter.exception;

import com.satish.fieldvalidator.domain.exceptions.DataConflictException;
import com.satish.fieldvalidator.domain.exceptions.MiscException;
import com.satish.fieldvalidator.domain.exceptions.NoDataException;
import com.satish.fieldvalidator.webcore.dto.HttpErrorResponse;
import com.satish.fieldvalidator.webcore.exception.CoreException;
import com.satish.fieldvalidator.webcore.exception.ErrorMessageSpec;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@RequiredArgsConstructor
public class RestAdapterExceptionHandler implements CoreException<ResponseEntity<HttpErrorResponse>> {
    private final ErrorMessageSpec errorMessageProducer;

    @Override
    public ResponseEntity<HttpErrorResponse> methodArgumentException(MethodArgumentNotValidException ex) {
        return ResponseEntity.badRequest().body(errorMessageProducer.dataBind(ex));
    }

    @Override
    public ResponseEntity<HttpErrorResponse> dataNotFoundException(NoDataException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessageProducer.noData(ex));
    }

    @Override
    public ResponseEntity<HttpErrorResponse> dataConflictException(DataConflictException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMessageProducer.dataConflict(ex));
    }

    @Override
    public ResponseEntity<HttpErrorResponse> exception(Exception ex, HttpServletRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageProducer.exception(ex, request));
    }

    @Override
    public ResponseEntity<HttpErrorResponse> miscException(MiscException ex, HttpServletRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageProducer.miscException(ex, request));
    }
}
