package com.satish.fieldvalidator.restadapter.util.constants;

public class APIPathConstant {
    private APIPathConstant() {}

    public static final String BASE_PATH = "/api/v1";
    public static final String ID_PATH = "/{id}";
    public static final String COLUMN_NAME = "/{columnName}";
    public static final String FIELD = "/fields";
    public static final String FIELD_VALIDATOR = "/fieldValidators";
    public static final String MESSAGE_RESPONSE = "/messageResponse";
    public static final String VALIDATOR_VALUE = "/validatorValue";
}
