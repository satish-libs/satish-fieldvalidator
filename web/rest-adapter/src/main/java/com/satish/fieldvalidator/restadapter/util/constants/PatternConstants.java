package com.satish.fieldvalidator.restadapter.util.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PatternConstants {

    public final static String DATE_TIME= "yyyy-MM-dd hh:mm:ss a";
    public final static String DATE = "yyyy-MM-dd";
}
