$(document).ready(function() {
    let fieldProps;
    const baseUrl = '/fieldValidatorUI'
    function listSetText(selector, inputValue) {
        if (selector === '.main-info') {
            $(`#field-add-tracker .lst ${selector}`).text(function() {
                return inputValue;
            });
        } else {
            $(`#field-add-tracker .lst .sub-info:nth-child(${selector})`).children('small').text(function() {
                return inputValue;
            });
        }
    }

    function constructField(controls) {
        return controls
        .filter(control => control.name !== "messageResponse" && control.name !== "validatorValue")
        .reduce((acc, control) => {
            acc[control.attributeName] = control.value;
            return acc;
        }, {
             fieldValidators: getFieldValidators()
        });
    }

    function onSubmit(controls) {
             $.ajax({
                      url: `${baseUrl}`,
                      type: 'POST',
                      data: JSON.stringify(constructField(controls)),
                      contentType: 'application/json; charset=utf-8',
                      success: function(value) {
                          const href = location.href.split('fieldValidatorUI')[0];
                          location.href = href + 'fieldValidatorUI';
                     },
                     error: function(errorResponse) {
                            if(errorResponse && errorResponse.responseText) {
                              $(`#fieldWithValidator form #fieldKey`).closest('.input-addon').after(errorResponse.responseText)
                              .siblings('.error-message')
                              .addClass('input-error')
                            } else {
                              toasterMessageBy(errorResponse.status)
                            }
                     }
            });
    }

    function checkAndSetText(id, inputValue) {
        switch (id) {
            case 'fieldName':
                listSetText('.main-info', inputValue);
                break;
            case 'fieldKey':
                listSetText(2, 'Key: ' + inputValue);
                break;
            case 'fieldLabel':
                listSetText(3, 'Label: ' + inputValue);
                break;
            default:
                break;
        }
    }

    $('#add-field')
        .on('blur', '.form-control', function() {
            checkAndSetText(this.id, this.value);
    });

    (function setFieldProps() {
        const {fieldKey, fieldName, fieldPlaceholder, fieldLabel, messageResponse, validatorValue} = fields;
        fieldProps = [fieldKey, fieldName, fieldPlaceholder, fieldLabel, messageResponse, validatorValue];
    })();

    validateOn('blur', 'form input', '#fieldWithValidator', fieldProps);

    validateOn('submit', 'form', '#fieldWithValidator', fieldProps, onSubmit);
});