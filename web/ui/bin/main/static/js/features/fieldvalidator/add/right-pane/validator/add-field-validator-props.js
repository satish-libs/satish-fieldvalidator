const messageValuePair = {messageResponse: 'message', validatorValue: 'value'};
function getFieldValidatorFrom(selector, fieldValidator) {
    $(selector).find('.form-group input').each((index, inputElement) => {
       let fieldValidatorProp = {};
       const fieldValidatorPropName = $(inputElement).attr('name').split('_')[1];
       fieldValidatorProp[messageValuePair[fieldValidatorPropName]] = $(inputElement).val();
       fieldValidator[fieldValidatorPropName] = fieldValidatorProp;

    });
    return fieldValidator;
}

function getFieldValidators() {
 let fieldValidators = []
 $('#validator-info .message-value-pair').each((index, selector) => {
     let fieldValidator = {};
     const validatorType = toValidatorType($(selector).find('.validator-type').text());
     fieldValidator.validator = validators.find(validator => validator.type === validatorType);
     fieldValidators.push(getFieldValidatorFrom(selector, fieldValidator));
 });
 return fieldValidators;
}