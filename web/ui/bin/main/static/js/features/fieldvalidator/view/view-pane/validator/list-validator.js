$(document).ready(function() {
   const parentSelector = '#fieldWithValidator';
   function deleteSuccess(fieldValidator) {
          if(fieldValidator && fieldValidator.id) {
                $(`#${fieldValidator.id}_validator_type .info .right-action input:checkbox`).prop('checked', fieldValidator.status)
          }
    }

    function softDelete(listId, toggleButton) {
         $.ajax({
                 url: `/fieldValidatorUI/field-validators/${listId}`,
                 type: 'DELETE',
                 success: deleteSuccess,
                 error: function(errorResponse) {
                    toasterMessageBy(errorResponse.status)
                 }
                });
    }

    function revertToggleCheck(toggleButton) {
         $(toggleButton).prop('checked', !toggleButton.prop('checked'));
    }

   $(parentSelector)
   .on('change', '.switch input', function() {
       const listId = $(this).closest('li').attr('id').split('_')[0];
       const toggleButton = $(this);
       revertToggleCheck(toggleButton)
        if(listId && !isNaN(listId)) {
            softDelete(listId, toggleButton)
        }
    });

    $('#fieldWithValidator li:first a .info')
      .addClass('active');

    $.getScript('/js/shared/widgets/list-item.js', function(v) {
        onItemClick('#fieldWithValidator');
    });
});