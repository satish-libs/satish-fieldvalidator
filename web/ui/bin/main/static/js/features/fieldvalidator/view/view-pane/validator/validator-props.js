$(document).ready(function() {
    const validator = '#validator';
    const baseUrl = '/fieldValidatorUI/field-validators';
    let fieldProps;
    function getFieldValidatorId () {
          const fieldValidatorId =  $(`${validator} .validator-types .info.active`).closest('li').attr('id').split('_')[0];
          if(fieldValidatorId && !isNaN(fieldValidatorId)) {
            return fieldValidatorId;
          }
          return null;
   }


   function deactivateArticleEdit(errorResponse) {
       const editForm =  $(`${validator} #validatorProps .widget-article-sub form`);
       const fieldValidatorId = getFieldValidatorId();
       if(fieldValidatorId && editForm.length > 0) {
          const articleProp = editForm.closest('.article-prop');
          if(articleProp.length > 0) {
               const articleContentId =  articleProp.attr('id');
               $(articleProp).load(`${baseUrl}/validator-article-contents/${fieldValidatorId || ''}/${articleContentId}`,
                 function(response, status, xhr) {
                      if(status === 'error') {
                          toasterMessageBy(xhr.status)
                       }
               });
          }
       }
    }

    function loadArticleResult(selector) {
             deactivateArticleEdit();
             const fieldValidatorId = getFieldValidatorId();
             const articleId = $(selector).parent().attr("id");
             const articleContent = $(selector).siblings();

             if(articleId && fieldValidatorId) {
                   let url = `${baseUrl}/validator-edit-frag/${fieldValidatorId}/${articleId}`;
                   $(selector).siblings().load(url, function(response, status, xhr) {
                          if(status === 'error') {
                              toasterMessageBy(xhr.status)
                           } else {
                                $(selector).remove();
                           }
                   });

             }
    }

    function setFieldProps() {
             const {messageResponse, validatorValue} = JSON.parse(JSON.stringify(fields));
             fieldProps = [ messageResponse, validatorValue];
    }


    function updateValidatorProp(fieldValidatorId, articleContentId, propUrl) {
      if(fieldValidatorId && articleContentId && propUrl) {
          $.ajax({
                  url: `${baseUrl}/${fieldValidatorId}/${articleContentId}/${propUrl}`,
                  type: 'PATCH',
                  contentType: 'application/json; charset=utf-8',
                  success: function(value) {
                      $(`#${articleContentId} .article-content`).replaceWith(value);
                  },
                  error: deactivateArticleEdit
              })
      }
    }

       inputCheckOn(validator, '.validator-props .validator-info form input', function() {
           return toValidatorType($(`#validator .info.active .main-info`).text());
       });
       
       activateEdit(validator, loadArticleResult);

       cancelEdit(validator, deactivateArticleEdit);

       setFieldProps();

       validateOn('blur', '.validator-props .validator-info form input', validator, fieldProps);

       validateOn('submit', '.validator-props .validator-info form', validator, fieldProps, function(fields) {
                const articleId = $(`${validator} form`).closest('.article-prop').attr("id");
                let propUrl;
                const field = fields[0];
                if(field.name === 'messageResponse') {
                    propUrl = `messageResponse/?newMessage=${field.value}`
                } else if(field.name === 'validatorValue') {
                    propUrl = `validatorValue/?newValue=${field.value}`
                }
                updateValidatorProp(getFieldValidatorId(), articleId, propUrl);
       });
});