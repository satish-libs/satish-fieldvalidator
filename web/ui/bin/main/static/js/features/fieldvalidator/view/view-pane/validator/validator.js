$(document).ready(function() {
$("#fieldWithValidator")
.on("click", "#widget-list-item .lst .info", function(e) {
    const validatorId =  $(this).closest("li").attr("id").split("_")[0];
    if(!isNaN(validatorId)) {
        $("#fieldWithValidator #validator .validator-info")
        .load("/fieldValidatorUI/field-validators/"+validatorId, function(response, status, xhr) {
               if(status === 'error') {
                   toasterMessageBy(xhr.status)
                }
        });
    }
})

function cancelNewFieldValidator() {
  let fieldId = getFieldId() || $('#feature-field-list li:first').attr('id').split('_')[0];
  if(fieldId) {
       $('#validator').load(`/fieldValidatorUI/field-validators/${fieldId}/cancel`, function( response, status, xhr ) {
           if(status !== 'error') {
                $(this).find("li:first .info")
                .addClass('active');
           } else {
                 toasterMessageBy(xhr.status)
           }
       });
  }
}

$('#validator')
    .on('click', '.cancel-add-item', cancelNewFieldValidator);

$('#validator')
    .on('click', '.button-group .btn-danger', cancelNewFieldValidator);
});
