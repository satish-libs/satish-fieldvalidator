const fields = {
        fieldKey: {
            name: 'fieldKey',
            attributeName: 'fieldKey',
            validators: [{
                type: 'REQUIRED',
                value: 'true',
                message: 'Field key is a required field'
            }, {
                type: 'MAX_LENGTH',
                value: '50',
                message: 'Field key cannot be greater than 50 characters'
            }, {
                type: 'MIN_LENGTH',
                value: '2',
                message: 'Field key cannot be less than 2 characters'
            }]
        },
        fieldName: {
            name: 'fieldName',
            attributeName: 'name',
            validators: [{
                type: 'REQUIRED',
                value: 'true',
                message: 'Field name is a required field'
            }, {
                type: 'MAX_LENGTH',
                value: '50',
                message: 'Field name cannot be greater than 50 characters'
            }, {
                type: 'MIN_LENGTH',
                value: '2',
                message: 'Field name cannot be less than 2 characters'
            }]
        },
        fieldLabel: {
            name: 'fieldLabel',
            attributeName: 'label',
            validators: [{
                type: 'REQUIRED',
                value: 'true',
                message: 'Field label is a required field'
            }, {
                type: 'MAX_LENGTH',
                value: '50',
                message: 'Field label cannot be greater than 50 characters'
            }, {
                type: 'MIN_LENGTH',
                value: '2',
                message: 'Field label cannot be less than 2 characters'
            }]
        },
        fieldPlaceholder: {
            name: 'fieldPlaceholder',
            attributeName: 'placeholder',
            validators: [{
                type: 'MAX_LENGTH',
                value: '100',
                message: 'Placeholder cannot be greater than 100 characters'
            }, {
                type: 'MIN_LENGTH',
                value: '5',
                message: 'Placeholder cannot be less than 5 characters'
            }]
        },
        messageResponse: {
            name: 'messageResponse',
            attributeName: 'message',
            validators: [{
                type: 'REQUIRED',
                value: 'true',
                message: 'Message response is a required field'
            },            {
                type: 'MAX_LENGTH',
                value: '255',
                message: 'Message response cannot be greater than 255 characters'
            }, {
                type: 'MIN_LENGTH',
                value: '5',
                message: 'Message response cannot be less than 5 characters'
            }]
        },
        validatorValue: {
            name: 'validatorValue',
            attributeName: 'value',
            validators: [{
                type: 'REQUIRED',
                value: 'true',
                message: 'Validator value is a required field'
            },            {
                type: 'MAX_LENGTH',
                value: '500',
                message: 'Validator value cannot be greater than 500 characters'
            }]
        }
}


