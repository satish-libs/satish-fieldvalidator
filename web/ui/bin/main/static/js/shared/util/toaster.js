    function toaster(toasterMessage, toasterType) {
        const toasterObj = {
           message: toasterMessage,
           type: toasterType,
           fadeIn: '',
           fadeOut: ''

        };
        toasterObj.fadeOut = fadeOutToaster.bind(toasterObj);
        toasterObj.fadeIn = fadeInToaster.bind(toasterObj);
        return toasterObj;
    }

    function fadeInToaster() {
       const toasterObj = this;
            $('.toaster-message')
             .toggleClass(toasterObj.type)
             .css('display', 'block')
             .animate({
                  width: '280px',
                  height: '100px',
                  opacity: '1'
              })
              .find('.sub-heading').text(function() {
                  return toasterObj.message;
              });
       return toasterObj;
    }

    function fadeOutToaster(timeout) {
    const toasterObj = this;
             setTimeout(() => {
                    $('.toaster-message')
                     .toggleClass(toasterObj.type)
                     .animate({
                         width: '0px',
                         height: '0px',
                         opacity: '1',
                     })
                     .find('.sub-heading').text(function() {
                         return '';
                     });
                     setTimeout(() => {
                           $('.toaster-message').css('display', 'none');
                     }, 300);
             }, timeout)
         return toasterObj;
    }

    function toasterMessageBy(status, message, timeout) {
      if( $('.toaster-message').css('display') === 'none'){
        timeout = timeout || 5000;
        if(navigator.onLine) {
             if(status === 0) {
                  toaster('Issue while connecting to the server', 'warning')
                  .fadeIn()
                  .fadeOut(timeout);
             } else if(status >= 500) {
                  toaster('Internal server error', 'danger')
                  .fadeIn()
                  .fadeOut(timeout);
             } else if(status >= 200 || status <= 300) {
                 toaster(message, 'success')
                 .fadeIn()
                 .fadeOut(timeout);
             } else if(status === 400) {
                 toaster(message, 'warning')
                 .fadeIn()
                 .fadeOut(timeout);
             }
        } else {
               toaster('No internet connection', 'warning')
                .fadeIn()
                .fadeOut(timeout);
        }
      }
    }
