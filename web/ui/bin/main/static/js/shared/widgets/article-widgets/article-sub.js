 function activateEdit(parentSelector, callBack) {
        $(parentSelector)
            .on('click', '.info .article-action', function() {
                callBack(this);
            });
 }

function cancelEdit(parentSelector, callBack) {
    if (parentSelector) {
        $(parentSelector)
            .on('click', '.info form .btn-danger', function() {
                 callBack(this);
            });
    }
}



