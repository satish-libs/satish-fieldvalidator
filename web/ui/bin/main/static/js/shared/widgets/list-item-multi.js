function toggleItem() {
    $(this).toggleClass('active');
}

function onItemClick(parentSelector) {

    if (parentSelector) {
        $(parentSelector)
            .on('click', '#list-item-multi li .info ', toggleItem);
    } else {
        $('#widget-list-item-multi li')
            .on('click', '.info', toggleItem);
    }
};
