function onItemClick(parentSelector) {
    $(parentSelector)
        .on('click', '#widget-list-item li a', highlightItem)
        .on('click', '.switch', function(e) {
            e.stopPropagation();
        });
}

function highlightItem() {
    const infoDiv = $(this).children(':first');
    if (!infoDiv.hasClass('active')) {
        $(this).closest("li").siblings().find('.active').removeClass('active');
        infoDiv.addClass('active');
    }
}