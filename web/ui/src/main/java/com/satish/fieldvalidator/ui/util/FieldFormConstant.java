package com.satish.fieldvalidator.ui.util;

public class FieldFormConstant {
    private FieldFormConstant() {
    }

    public static final class FieldKey {
        private FieldKey() {
        }

        public static final String ID = "fieldKey";
        public static final String PLACEHOLDER = "Enter unique identifier for the field";
        public static final String TYPE = "text";
        public static final int MAX_LENGTH = 255;
        public static final int MIN_LENGTH = 2;
        public static final String NAME = "fieldKey";
    }

    public static final class FieldName {
        private FieldName() {
        }

        public static final String ID = "fieldName";
        public static final String PLACEHOLDER = "Enter name for the field";
        public static final String TYPE = "text";
        public static final int MAX_LENGTH = 50;
        public static final int MIN_LENGTH = 2;
        public static final String NAME = "fieldName";
    }

    public static final class FieldLabel {
        private FieldLabel() {
        }

        public static final String ID = "fieldLabel";
        public static final String PLACEHOLDER = "Enter label for the field";
        public static final String TYPE = "text";
        public static final int MAX_LENGTH = 50;
        public static final int MIN_LENGTH = 2;
        public static final String NAME = "fieldLabel";
    }

    public static final class FieldPlaceholder {
        private FieldPlaceholder() {
        }

        public static final String ID = "fieldPlaceholder";
        public static final String PLACEHOLDER = "Enter placeholder for the field";
        public static final String TYPE = "text";
        public static final int MAX_LENGTH = 100;
        public static final int MIN_LENGTH = 5;
        public static final String NAME = "fieldPlaceholder";
    }
}
