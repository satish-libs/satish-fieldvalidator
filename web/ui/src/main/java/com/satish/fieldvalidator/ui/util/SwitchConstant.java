package com.satish.fieldvalidator.ui.util;

import com.satish.fieldvalidator.mvcadapter.uimodel.Switch;

public class SwitchConstant {
    private SwitchConstant() {
    }

    public static final class StatusSwitch {

        private StatusSwitch() {
        }

        public static Switch[] defaultValue() {
            return allChecked();
        }

        public static Switch[] allChecked() {
            return checkOn(0);
        }

        public static Switch[] activeChecked() {
            return checkOn(1);
        }

        public static Switch[] inactiveChecked() {
            return checkOn(2);
        }

        private static Switch[] checkOn(int index) {
            Switch[] switches = defaultSwitch();
            switches[index].setChecked(true);
            return switches;
        }

        private static Switch[] defaultSwitch() {
            return new Switch[] { new Switch("1", "-", "All", false), new Switch("2", "true", "Active", false),
                    new Switch("3", "false", "Inactive", false) };
        }
    }
}
