$.when(
    $.getScript('/js/shared/widgets/list-item-multi.js'),
    $.getScript('/js/features/fieldvalidator/add/right-pane/validator/add-field-validator-props.js'),
    $.Deferred(function(deferred) {
        $(deferred.resolve);
    })
).done( function() {
    $(document).ready(function() {
        const parentSelector = '#fieldWithValidator';
        let fieldProps;
        const baseUrl = "/fieldValidatorUI";

        function validatorProps(validator) {
            const validatorValue = `<div class="form-group">
                                         <div class="input-addon">
                                            <label class="m-lt-12p" for="${validator.id}_validator_value"><strong>Validator Value</strong><sup class="required-field">*</sup></label>
                                             <input name="${validator.id}_validatorValue" placeholder="Enter value for provided validator" id="${validator.id}_validator_value" type="text" class="form-control">
                                         </div>
                                     </div>`;
            return `<div id=${validator.id}_validator_info class="message-value-pair m-bm-25p">
                            <h4 class="validator-type text-center">${toSentenceCase(validator.type)}</h4>
                                <fieldset>
                                   <div class="form-group">
                                      <div class="input-addon">
                                        <label class="m-lt-12p" for="${validator.id}_message_response"><strong>Message Response</strong><sup class="required-field">*</sup></label>
                                          <input name="${validator.id}_messageResponse" placeholder="Enter message response for provided validator" id="${validator.id}_message_response" type="text" class="form-control">
                                      </div>
                                    </div>
                                    ${(validator.dataType !== 'BOOLEAN' && validatorValue) || ''}
                                 </fieldset>
                        </div>`;
        }

        function buttonGroup() {
            return `<div class="button-group">
                        <button type="submit" class="btn btn-primary">Add field validator</button>
                        <button type="button" class="btn btn-danger">Cancel</button>
                    </div>`;
        }

        function getValidatorBy(validatorId) {
              return validators.find(validator => +validator.id === +validatorId);
        }

        function addToFormArray(validatorId) {
               $('#validator-info')
                 .append(validatorProps(getValidatorBy(validatorId)));
        }

        function removeFromFormArray(validatorId) {
            $(`#${validatorId}_validator_info`).remove();
        }

        function toggleButtonGroup() {
            if ($(`${parentSelector} #validator form`).get(0)) {
                const messageValuePairLength = $(`${parentSelector} #validator .message-value-pair`).length;
                if ($(`${parentSelector} #validator form .button-group`).length === 0 && messageValuePairLength > 0) {
                    addButtonGroup();
                } else if (messageValuePairLength === 0) {
                    removeButtonGroup();
                }
            }
        }

        function addButtonGroup() {
            $(`${parentSelector} #validator form .card-footer`)
                .append(buttonGroup());
        }

        function removeButtonGroup() {
            $(`${parentSelector} #validator form .card-footer`)
                .children().remove();
        }

        function onSubmit(fields) {
                const fieldId = getFieldId() || $('#feature-field-list li:first').attr('id').split('_')[0];
                const fieldValidators = getFieldValidators();
                 $.ajax({
                          url: `${baseUrl}/${fieldId}/field-validator`,
                          type: 'POST',
                          data: JSON.stringify(fieldValidators),
                          contentType: 'application/json; charset=utf-8',
                          success: function(validatorInfo){
                              $('#validator').children().replaceWith(validatorInfo);
                           },
                           error: function(response) {
                               toasterMessageBy(response.status)
                           }
                });
        }

        (function setFieldProps() {
                 const {messageResponse, validatorValue} = JSON.parse(JSON.stringify(fields));
                 fieldProps = [ messageResponse, validatorValue];
        })();

       validateOn('blur', '#validator form input', parentSelector, fieldProps);

       validateOn('submit', '#validator form.add-field-validator', parentSelector, fieldProps, onSubmit);

       onInActiveItemClick(parentSelector, function() {
                const validatorId = $(this).closest('li').attr('id').split('_')[0];
                addToFormArray(validatorId);
                toggleButtonGroup();
       });

        onActiveItemClick(parentSelector, function() {
                const validatorId = $(this).closest('li').attr('id').split('_')[0];
                removeFromFormArray(validatorId);
                toggleButtonGroup();
         });

        inputCheckOn(parentSelector, 'form .message-value-pair input', function() {
            return toValidatorType($(this).closest('.message-value-pair').find('.validator-type').text());
        });
    });
});
