$(document).ready(function() {
    const searchSelector = '#feature-field-list .search-bar input';
    const switchSelector = '#feature-field-list .switch-field input';
    const toggleButtonSelector = '.switch input';
    const activeList = '#feature-field-list .lst li .active';

    function scrollToActiveList() {
        if($(activeList).closest('li').css('display') !== 'none') {
            document.querySelector(activeList).scrollIntoView({behavior: "smooth"})
        }
    }

    function filterList(switchValue, searchValue) {
        $('#feature-field-list li')
            .filter(function() {
                const listText = $(this).text().trim().toLowerCase();
                const checkedToggle = $(this).find('.switch input').prop('checked');
                const showByToggleStatus = switchValue === '-' ? true : (switchValue === 'true') === checkedToggle;
                const showItem = showByToggleStatus && listText.includes(searchValue);
                $(this).toggle(showItem);
            });
    }

    $(searchSelector).keyup(function() {
        const searchText = $(this).val().toLowerCase().trim();
        filterList($(`${switchSelector}:checked`).val(), searchText);
        if(!searchText) {
             scrollToActiveList();
        }
    });

   function deleteSuccess(field) {
          if(field && field.id) {
                const switchValue = $(`${switchSelector}:checked`).val();
                const searchValue = $(searchSelector).val().toLowerCase().trim();
                $(`#${field.id}_field_list .info .right-action input:checkbox`).prop('checked', field.status)
                filterList(switchValue, searchValue);
          }
    }


    function softDelete(listId, toggleButton) {
         $.ajax({
             url: `/fieldValidatorUI/${listId}`,
             type: 'DELETE',
             success: deleteSuccess,
             error: function(errorResponse) {
                toasterMessageBy(errorResponse.status)
             }
         });
    }

    function revertToggleCheck(toggleButton) {
         $(toggleButton).prop('checked', !toggleButton.prop('checked'));
    }

    $('#feature-field-list')
    .on('change', toggleButtonSelector, function() {
       const listId = $(this).closest('li').attr('id').split('_')[0];
       const toggleButton = $(this);
       revertToggleCheck(toggleButton)
        if(listId && !isNaN(listId)) {
            softDelete(listId, toggleButton)
        }
    });

    $(switchSelector).click(function() {
        filterList($(this).val(), $(searchSelector).val().toLowerCase().trim());
        scrollToActiveList();
    });

    (function () {
        const fieldId = getFieldId();
        if(fieldId) {
             $(`#${fieldId}_field_list`)
             .find('.info')
             .addClass('active')
        } else {
            $('#feature-field-list li:first .info')
            .addClass('active')
        }
        scrollToActiveList();
    })();

});
