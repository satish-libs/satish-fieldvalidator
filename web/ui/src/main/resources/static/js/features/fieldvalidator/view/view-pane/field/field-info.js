$(document).ready(function() {
    const baseUrl = '/fieldValidatorUI';
    const fieldInfo = '#field-info';
    let fieldId;
    let fieldProps;

    function deactivateArticleEdit() {
       const editForm =  $(`${fieldInfo} form`);
       if(fieldId && editForm.length > 0) {
          const articleProp = editForm.closest('.article-prop');
          if(articleProp.length > 0) {
               const articleContentId =  articleProp.attr('id');
               $(articleProp).load(`${baseUrl}/field-article-contents/${fieldId || ''}/${articleContentId}`,
               function(response, status, xhr) {
                 if(status === 'error') {
                     toasterMessageBy(xhr.status)
                  }
               });
          }
       }
    }

    function loadArticleResult(selector) {
             deactivateArticleEdit();
             const articleId = $(selector).parent().attr("id");
             if(articleId && fieldId) {
                   let url = `${baseUrl}/field-edit-frag/${fieldId}/${articleId}`;
                   $(selector).siblings().load(url, function(response, status, xhr) {
                     if(status === 'error') {
                         toasterMessageBy(xhr.status);
                     } else {
                           $(selector).remove();
                     }
                   });
             }
    }

    function updateFieldInfo(fieldId, articleContentId, field) {
      if(fieldId && articleContentId && field && field.column) {
          $.ajax({
                  url: `${baseUrl}/${fieldId}/${articleContentId}`,
                  type: 'PATCH',
                  contentType: 'application/json; charset=utf-8',
                  data: JSON.stringify({
                    columnName:field.column,
                    value:field.value
                  }),
                  success: function(value) {
                      $(`#${articleContentId} .article-content`).replaceWith(value);
                      $.get(`${baseUrl}/list-items/${fieldId}`, function(value) {
                         $(`#${fieldId}_field_list`).replaceWith(value);
                          $(`#${fieldId}_field_list`)
                         .find('.info')
                         .addClass('active');
                      });
                  },
                  error: function(error) {
                    if(error && error.status === 409 && error.responseText) {
                      $(`#${articleContentId} form .input-addon`).after(error.responseText)
                      .siblings('.error-message')
                      .addClass('input-error')
                    } else {
                        toasterMessageBy(error.status)
                    }
                  }
              })
      }
    }

    function fieldIdFromList() {
      const fieldId = $("#field-info").closest(".field-info").siblings(".field-list").find(".lst li").attr("id").split("_")[0];
      if(fieldId && !isNaN(fieldId)) {
        return fieldId;
      }
      return null;
    }

     (function setFieldProps() {
            const {fieldKey, fieldName, fieldLabel, fieldPlaceholder} = JSON.parse(JSON.stringify(fields));
            fieldKey.column = 'field_key';
            fieldName.column = 'name';
            fieldLabel.column = 'label';
            fieldPlaceholder.column = 'placeholder';
            fieldProps = [ fieldKey, fieldName, fieldLabel, fieldPlaceholder];
    })();

    fieldId = getFieldId() || fieldIdFromList();

    validateOn('blur', '.article-prop form input', fieldInfo, fieldProps);

    validateOn('submit', '.article-prop form', fieldInfo, fieldProps, function(fields) {
            const articleId = $(`${fieldInfo} form`).closest('.article-prop').attr("id");
            updateFieldInfo(fieldId, articleId, fields[0]);
    });

    activateEdit(fieldInfo, loadArticleResult);

    cancelEdit(fieldInfo, deactivateArticleEdit);
});