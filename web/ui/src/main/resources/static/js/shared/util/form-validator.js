
function validateOn(trigger, triggerOn, parentSelector, fields, onSubmitSuccess) {
     if(fields && fields.length > 0) {
         if (parentSelector) {
                $(parentSelector)
                    .on(trigger, triggerOn, validate(fields, onSubmitSuccess));
            }
        } else {
            throw new Error("fields initialization failed! Provide field/s details.");
     }
}


function validate(fields, onSubmitSuccess) {
    return function(submitEvent) {
        submitEvent.preventDefault();
        const validFields = controlsToFields.call(this, $(this).serializeArray(), fields);
        !validFields.isInvalid && onSubmitSuccess && onSubmitSuccess(validFields[0]);
    }
}

function controlsToFields(controls, fields) {
        let isValid = true;
        return {...[controls.map(control => {
               if(control && control.name) {
                    const field = {...fields.find(value => control.name.includes(value.name) )};
                    const validators = field.validators;
                    if(field) {
                          field.value = control.value;
                          field.controlName = control.name;
                          if(validateField.call(this, validators, control)) {
                            clearErrorMessage(control.name);
                            return field;
                          } else {
                            isValid = false;
                          }
                    }
                 }
        }).filter(control => control)], isInvalid: !isValid};
}


function validateField(validators, control) {
    if(validators && validators.length > 0) {
        const isValid = validateRequired.call(this, validators, control) && validateRest.call(this, validators, control);
        return isValid;
    }
}

function validateRequired(validators, control) {
    const required = validators.find(validator => validator.type === "REQUIRED");
    if(required && !control.value) {
        showErrorMessage.call(this, control.name)(required);
        return false;
    }
    return true;
}

function validateRest(validators, control) {
    if(control && control.value) {
         const value = control.value;
         const errorMessage = showErrorMessage.call(this, control.name);
         for(let index = 0; index < validators.length; index++) {
             const validator = validators[index];
             if(!validator.value) {
                throw new Error("value for validator is empty.");
             }
             switch(validator.type) {
             case "MAX_LENGTH" :
                 if(value.length > validator.value) {
                    errorMessage(validator);
                    return false;
                 }
             break;
             case "MIN_LENGTH" :
                 if(value.length < validator.value) {
                    errorMessage(validator);
                    return false;
                 }
             break;
             case "PATTERN" :
                 if(!value.match(validator.value)) {
                    errorMessage(validator);
                    return false;
                 }
             break;
             case "MAX" :
                 if( !isNaN(value) && value > validator.value) {
                    errorMessage(validator);
                    return false;
                 }
             break;
             case "MIN" :
                 if( !isNaN(value) && value< validator.value) {
                    errorMessage(validator);
                    return false;
                 }
             break;
             }
          }
    }
     return true;
}

function clearErrorMessage(controlName) {
    const inputAddOn = $(`input[name = ${controlName}]`).closest('.input-addon');
    const errorMessage =  inputAddOn.next(".input-error");
    if(errorMessage.length > 0) {
        errorMessage.remove();
    }
}

function showErrorMessage(controlName) {
    return function (validator) {
            if(validator.message) {
                 const inputAddOn = $(`input[name = ${controlName}]`).closest('.input-addon');
                 const errorMessage =  inputAddOn.next(".input-error");
                 if(errorMessage.length === 0) {
                 inputAddOn
                          .after(`
                                    <span class="input-error">${validator.message}</span>
                          `);
                 } else {
                    errorMessage.text(validator.message);
                 }
             }
    }
}

const validatorTypes = [
    {
        key: 'MAX_LENGTH',
        type: 'INTEGER'
    },
    {
        key: 'MIN_LENGTH',
        type: 'INTEGER'
    },
    {
        key: 'MAX_VALUE',
        type: 'DECIMAL'
    },
    {
        key: 'MIN_VALUE',
        type: 'DECIMAL'
    },
    {
        key: 'PATTERN',
        type: 'ANY'
    }
]

function inputCheckOn(parentSelector, inputSelector, callBack) {
   $(parentSelector)
    .on('input', inputSelector, function() {
        const controlName = $(this).attr("name");
        const validatorTypeKey = callBack.call(this);
        const inputValue = $(this).val();
        if(controlName.includes("validatorValue") && validatorTypeKey && validatorTypes) {
            const validatorType = validatorTypes.find(validator => validator.key === validatorTypeKey);
            if(validatorType) {
                 if (validatorType.type === 'INTEGER') {
                    $(this).val(replaceToInteger(inputValue));
                 } else if (validatorType.type === 'DECIMAL') {
                    $(this).val(replaceToDecimal(inputValue));
                 }
             } else {
               $(this).val(null);
             }
        }
        const isValidControl = controlName.includes('validatorValue') || controlName.includes('messageResponse');
        if(!isValidControl || !validatorTypeKey || !validators)  {
          $(this).val(null);
        }
    });
}