const replaceToInteger = value => {
   return value.replace(/\D/g, '');
};

const replaceToDecimal = value => {
   if (value === '.') {
       return '0.';
   }
   return (value.match(/^\d*\.?\d*/g) || [''])[0];
};

function toSentenceCase(text) {
  if(!text) {
     return '';
  }
  return (text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase())
          .replace('_', ' ');
}

function toValidatorType(text) {
  if(!text) {
    return null;
  }
  return text.toUpperCase().replace(' ', '_');
}