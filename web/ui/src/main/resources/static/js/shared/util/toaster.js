    function toaster(toasterMessage, toasterType, toasterHeading) {
        const toasterObj = {
           heading: toasterHeading || '',
           message: toasterMessage,
           type: toasterType,
           fadeIn: '',
           fadeOut: ''

        };
        toasterObj.fadeOut = fadeOutToaster.bind(toasterObj);
        toasterObj.fadeIn = fadeInToaster.bind(toasterObj);
        return toasterObj;
    }

    function fadeInToaster() {
       const toasterObj = this;
       const toaster = $('.toaster-message');
       toaster
             .toggleClass(toasterObj.type)
             .css('display', 'block')
             .animate({
                  width: '320px',
                  minHeight: '120px',
                  opacity: '1'
              });
       toaster
              .find('.heading').text(function() {
                  return toasterObj.heading;
              });
       toaster
              .find('.sub-heading').text(function() {
                  return toasterObj.message;
              });
       return toasterObj;
    }

    function fadeOutToaster(timeout) {
    const toasterObj = this;
             setTimeout(() => {
               const toaster = $('.toaster-message');
               toaster
                     .toggleClass(toasterObj.type)
                     .animate({
                         width: '0px',
                         height: '0px',
                         minHeight: '0px',
                         opacity: '1',
                     });
                toaster
                     .find('.heading').text(function() {
                         return '';
                     });
                 toaster
                     .find('.sub-heading').text(function() {
                         return '';
                     });
                     setTimeout(() => {
                           $('.toaster-message').css('display', 'none');
                     }, 300);
             }, timeout)
         return toasterObj;
    }

    function toasterMessageBy(status, message, timeout) {
      if( $('.toaster-message').css('display') === 'none'){
        timeout = timeout || 5000;
        if(navigator.onLine) {
             if(status === 0) {
                  toaster('Issue while connecting to the server', 'warning', "Server down")
                  .fadeIn()
                  .fadeOut(timeout);
             } else if(status >= 500) {
                  toaster('Internal server error', 'danger', "Server problem")
                  .fadeIn()
                  .fadeOut(timeout);
             } else if(status >= 200 && status <= 300) {
                 toaster(message || "Action completed successfully", 'success', "Success")
                 .fadeIn()
                 .fadeOut(timeout);
             } else if(status === 400) {
                 toaster(message || "Invalid request", 'warning', "Bad request")
                 .fadeIn()
                 .fadeOut(timeout);
             } else if(status === 401) {
                 toaster("This operation has been blocked(Configure to permit operation url)", 'danger', "Unauthorized")
                 .fadeIn()
                 .fadeOut(timeout);
               } else if(status === 403) {
                 toaster("This operation has been blocked(Configure to permit operation url)", 'danger', "Forbidden")
                 .fadeIn()
                 .fadeOut(timeout);
               } else if(status === 404) {
                 toaster("Cannot operate on requested operation(Something wrong with configuration)", 'danger', "Operation does not exist")
                 .fadeIn()
                 .fadeOut(timeout);
               } else {
                    $('.toaster-message').css('display', 'none'); 
               }
        } else {
               toaster('No internet connection', 'warning', "Offline")
                .fadeIn()
                .fadeOut(timeout);
        }
      }
    }
