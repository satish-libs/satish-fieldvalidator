function getFieldId() {
       const splittedHREF = (window.location.href).split("/");
       if(splittedHREF.length == 5 && splittedHREF[4]) {
         return splittedHREF[4].replace("#", "")
                .replace(/(\?[\D\d]*)/, '');
       } else {
         return null;
      }
}