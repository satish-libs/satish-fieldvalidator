function onInActiveItemClick(parentSelector, callBack) {
    if (parentSelector) {
        $(parentSelector)
            .on('click', '#list-item-multi li .info:not(.active)', function() {
                $(this).addClass('active');
                if(callBack) {
                     callBack.call(this);
                }
            });
    }
};

function onActiveItemClick(parentSelector, callBack) {
    if (parentSelector) {
        $(parentSelector)
            .on('click', '#list-item-multi li .info.active', function() {
                $(this).removeClass('active');
                if(callBack) {
                    callBack.call(this);
                }
            });
    }
};